-- LUALOCALS < ---------------------------------------------------------
local default, io, minetest, pairs, print
    = default, io, minetest, pairs, print
local io_close, io_open
    = io.close, io.open
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()
local modpath = minetest.get_modpath(modname)

local privname = "skins"
minetest.register_privilege(privname, "Use custom skin, if present on server")

-- Helper to keep track of which skins exist.  These can be
-- memoized permanently, since texture assets cannot be added
-- to the server (and hence the clients) after server load time.
local cache = { }
local function findskin(name)
	if not name then return end

	-- Check for an already-cached file, or a
	-- negatively-cached missing one.
	local file = cache[name]
	if file then return file end
	if file ~= nil then return end

	-- Calculate and validate skin name from player name.
	-- Only alphanumerics are allowed; players with weird
	-- characters in their names are not allowed, to avoid
	-- path exploits.  Try to open the file to check if
	-- it exists.
	file = modname .. "_player_" .. name .. ".png"
	local f = name:gsub("%w", "") == "" and
	io_open(modpath .. "/textures/" .. file)
	if f then
		io_close(f)

		-- Save a positive cache result with
		-- the computed filename.
		cache[name] = file
		return file
	end

	-- Save a negative cache result.
	cache[name] = false
end

-- Helper to check what skin to apply to a player, and
-- change it if it's not already set correctly.
local applied = { }
local function checkskin(player)
	local name = player:get_player_name()
	if not name then return end
	local privs = minetest.get_player_privs(name)

	-- Start with a base skin.  This skin is always underneath
	-- the custom one, so that (1) players can't go "invisible"
	-- with a transparent skin, and (2) players can create
	-- minimal skins if they just want small tweaks of the default.
	local skin = minetest.setting_get(modname .. "_default")
	or "character.png"

	-- Check for custom skin for player, if allowed.
	if privs[privname] then
		local cust = findskin(name)
		if cust then 
			skin = skin .. "^" .. modname .. "_base_mask.png"
			.. "^[makealpha:255,3,254^" .. cust
		end
	end

	-- Apply modifications if the player is missing certain "basic"
	-- privs that would strongly affect gameplay.  This provides
	-- visual cues to other players as to why these players cannot
	-- talk or otherwise interact with them.
	--   - no "interact" = missing arms
	--   - no "shout" = censor bar over the mouth
	if not privs.shout then skin = skin
		.. "^" .. modname .. "_nopriv_shout.png"
	end
	if not privs.interact then skin = skin
		.. "^" .. modname .. "_nopriv_interact.png^[makealpha:255,3,254"
	end

	-- If the appropriate skin is already applied, skip re-applying
	-- it; changing the skin apparently causes model animation skips.
	local cur = applied[name]
	if skin == cur then return end

	-- Log and apply the skin.  Remember what skin we applied
	-- so we don't needlessly reapply it next time.
	print(modname .. ": applying skin \""
		.. skin .. "\" for \"" .. name .. "\".")
	default.player_set_textures(player, { skin })
	applied[name] = skin
end

-- Periodically check player skins in a loop.  This is needed
-- constantly during gameplay because players may gain/lose
-- privs at any time, we don't want to have them need to log out
-- to update their appearance, and it's too much of a bother to
-- properly hook exactly the correct places in the engine to
-- detect this.
local function checkloop()
	for k, v in pairs(minetest.get_connected_players()) do
		checkskin(v)
	end
	minetest.after(1, checkloop)
end
checkloop()

-- Update skins immediately when players join.
minetest.register_on_joinplayer(checkskin)

-- Clear skins when players exit, so we correctly re-apply them
-- when they log back in again later.
minetest.register_on_leaveplayer(function(player)
		applied[player:get_player_name()] = nil
	end)
