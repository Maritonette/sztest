-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, sz_mininodes, sz_table
    = minetest, pairs, sz_mininodes, sz_table
-- LUALOCALS > ---------------------------------------------------------

local reg = sz_mininodes.register

sz_mininodes.reregister_defer(function()
	-- Miscellaneous solid blocks, which make plenty of
	-- sense and translate into stairs pretty well.
	-- Some of these might already be stairs in the base
	-- game by now, but we make sure they're registered.
	reg("default", "cactus")
	reg("default", "desert_stone")
	reg("default", "obsidian")
	reg("default", "desert_stonebrick")
	reg("default", "sandstonebrick")
	reg("default", "clay")
	reg("default", "mossycobble")
	reg("default", "coalblock")
	reg("default", "steelblock")
	reg("default", "copperblock")
	reg("default", "bronzeblock")
	reg("default", "mese")
	reg("default", "goldblock")
	reg("default", "diamondblock")
	reg("default", "ice")
	reg("default", "snowblock")

	-- Some soft materials that don't make a lot of sense,
	-- but could be pretty cool anyway.  These will fall
	-- just like their prototypes, and keep their shape, even
	-- though the falling node might not appear quite right
	-- as it's falling.
	reg("default", "gravel")
	reg("default", "sand")
	reg("default", "desert_sand")

	-- Wool in all colors of the rainbow.
	for k, v in pairs(sz_table.copy(minetest.registered_nodes)) do
		if v.groups.wool and k:startswith("wool:") then
			reg("wool", k:sub(6))
		end
	end
end)
