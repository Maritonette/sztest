CHRONOKINETICS:
A system to create things that continue to simulate while unloaded.
It's too much to try to simulate everything by time delta when loaded,
i.e. while a furnace is manageable, complex multi-node machines such
as pinion-driven platform assemblies would be largely impossible.
Instead, we should make "continues running while unloaded" a privilege,
and only available for some select nodes (such as cooking vessels),
by providing special alternate versions of each node with a different
abm that tracks get_gametime() instead of just +1 second on each abm.

