LOW-VOLTAGE DIFFERENTIAL SIGNAL SYSTEM:
More balanced replacement for mesecons, provides a "signalling"
system for things like sensors, and combinational logic.  Includes
cables (single direction based on rotation like the axle), a junction
node for direction changes and fan-out, and a NOR gate.  Maybe include
mesecons API hooks for those who REALLY want a bridge.  Integrate with
rotary system via a "clutch relay" that requires a signal, plus rotary
power, to disengage an attached gearbox.
- Taking a cue from Technic's "Switching Station", we could have a
  single node that "enables" an electronic network within its own
  mapblock (use cosmosthesium to find boundaries).  Nearby blocks may
  desync, so we'd need either buffers, or maybe transfer to alternate
  power for long-distance transmission.

