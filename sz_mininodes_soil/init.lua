-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, pairs, sz_facedir, sz_mininodes, sz_pos,
      sz_table
    = ipairs, minetest, pairs, sz_facedir, sz_mininodes, sz_pos,
      sz_table
-- LUALOCALS > ---------------------------------------------------------

local reg = sz_mininodes.register

sz_mininodes.reregister_defer(function()
	-- Dirt stairs and slabs that can grow grass and snow.
	reg("default", "dirt")
	reg("default", "dirt_with_grass", "dirt")
	reg("default", "dirt_with_dry_grass", "dirt")
	reg("default", "dirt_with_snow", "dirt")

	-- Register custom texture overlays for material hanging down
	-- the side of the node for the multiple upper edges.
	for i, v in ipairs({
			"grass",
			"dry_grass",
			"snow"
			}) do
		sz_mininodes.custom_overlay_set("default_dirt.png^default_" .. v .. "_side.png",
			"sz_mininodes_soil_" .. v .. ".png", "default_dirt.png")
	end
end)

-- Logic for the grass growing function, that takes into account
-- the lighting and other environment of the node, plus the facing
-- direction, and appropriate geometric transformations.
local function grassgrowcheck(pos)
	pos = sz_pos:new(pos)

	-- Only upright stairs and slabs can grow grass.
	local node = pos:node_get()
	if not node then return end
	local facedir = sz_facedir:from_param(node.param2)
	if not facedir:top():eq(sz_pos.dirs.u) then

		-- Certain non-top-upright stairs and anulettes can be rotated such
		-- that their geometry is the same, but the textures are
		-- in the correct orientation.
		if (node.name:startswith("stairs:stair_")
			or node.name:startswith("sz_mininodes:anulette_"))
		and facedir:front():eq(sz_pos.dirs.u) then
			facedir = sz_facedir:from_vectors(
				facedir:bottom(), facedir:front())

		-- For dentils, chamfers, and concave corner stairs, the transformation
		-- is also possible, but a bit more complex.
		elseif (node.name:startswith("sz_mininodes:dentil_")
			or node.name:startswith("sz_mininodes:chamfer_")
			or node.name:startswith("sz_mininodes:concave_")) then

			-- Rotate CCW
			if facedir:front():eq(sz_pos.dirs.u) then
				facedir = sz_facedir:from_vectors(
					facedir:left(), facedir:front())

			-- Rotate CW
			elseif facedir:right():eq(sz_pos.dirs.u) then
				facedir = sz_facedir:from_vectors(
					facedir:bottom(), facedir:right())

			-- No rotation possible.
			else return end

			-- All the rest cannot be rotated such that the textures face the right
			-- direction without changing geometry, so leave them be.
		else return end
	end

	-- Node immediately above must be transparent and not liquid,
	-- or grass is not allowed.  If the node above is an unloaded
	-- mapblock, then grass may stay (for now) but not grow.
	local above = pos:add(sz_pos.dirs.u)
	local adef = above:nodedef()
	if above:node_get().name == "ignore" then return false, true end
	if not adef or adef.liquidtype ~= "none" or
	(not adef.sunlight_propagates and adef.paramtype ~= "light") then
		return
	end

	-- If the node above does not provide sufficient light, then
	-- grass may stay, but is not allowed to grow anew.
	if above:light() < 13 then return false, true end

	-- Grass is allowed to grow; also return the param2 to use for
	-- the new node, in case it's a staircase that needs to be rotated.
	return true, true, facedir.param
end

-- Register the ABM for all dirt nodes capable of growing grass or snow.
local dirtnodes = sz_table:new()
local grassnodes = sz_table:new()
local drygrassnodes = sz_table:new({"group:dry_grass", "default:dirt_with_dry_grass"})
for k, v in pairs(minetest.registered_nodes) do
	if (k:startswith("stairs:stair_") or k:startswith("stairs:slab_")
		or k:startswith("sz_mininodes:")) and k:endswith("_dirt")
	and minetest.registered_nodes[k .. "_with_grass"]
	and minetest.registered_nodes[k .. "_with_dry_grass"]
	and minetest.registered_nodes[k .. "_with_snow"] then
		dirtnodes:insert(k)
		grassnodes:insert(k .. "_with_grass")
		grassnodes:insert(k .. "_with_dry_grass")
		grassnodes:insert(k .. "_with_snow")
		drygrassnodes:insert(k .. "_with_dry_grass")
	end
end
minetest.register_abm({
		nodenames = dirtnodes,
		interval = 6,
		chance = 67,
		catch_up = false,
		action = function(pos, node)
			pos = sz_pos:new(pos)
			local cangrow, canstay, param2 = grassgrowcheck(pos)
			if cangrow then
				local with = "grass"
				local aname = pos:add(sz_pos.dirs.u):node_get().name
				if aname == "default:snow" or aname == "default:snowblock" then
					with = "snow"
				elseif pos:node_near(1, drygrassnodes) then
					with = "dry_grass"
				end
				pos:node_set({
					name = node.name .. "_with_" .. with,
					param2 = param2
				})
			end
		end
	})
minetest.register_abm({
		nodenames = grassnodes,
		interval = 8,
		chance = 50,
		catch_up = false,
		action = function(pos, node)
			pos = sz_pos:new(pos)
			local cangrow, canstay = grassgrowcheck(pos)
			if not canstay then
				pos:node_set({
					name = node.name
						:gsub("_with_grass", "")
						:gsub("_with_dry_grass", "")
						:gsub("_with_snow", ""),
					param2 = node.param2
				})
			end
		end
	})
