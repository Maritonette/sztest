-- LUALOCALS < ---------------------------------------------------------
local default, minetest, pairs, sz_facedir, sz_pos, sz_rotary, sz_table
    = default, minetest, pairs, sz_facedir, sz_pos, sz_rotary, sz_table
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local rot_pw_name = modname .. ":rotor_powered"
local rot_nopw_name = modname .. ":rotor"

local rot_anim_time = 2
local rotor_interval = 2

-- Helper to test for a node at "peer" that is "stuck to" a
-- node at "pos."  Wallmounted nodes are "stuck" to others,
-- as are facedir nodes in the "facedirmounted" group.
local function nodestuckto(pos, peer)
	local def = peer:nodedef()
	if not def then return false end
	if def.paramtype2 == "wallmounted" then
		local pn = peer:node_get()
		return sz_pos:from_wallmounted(pn.param2):eq(pos:sub(peer))
	elseif def.paramtype2 == "facedir" then
		if not def.groups or not def.groups.facedirmounted then
			return false
		end
		local pn = peer:node_get()
		return sz_facedir:from_param(pn.param2):front()
			:eq(peer:sub(pos))
	end
end

-- Function to handle rotor spinning.
local function rotor_spin(pos)
	pos = sz_pos:new(pos)
	local now = minetest.get_gametime()

	-- Reset the node timer at this location.
	pos:timer():start(rotor_interval)

	local jammed = false
	local oldnodes = sz_table:new()
	local newnodes = sz_table:new()

	local node = pos:node_get()
	local face = sz_facedir:from_param(node.param2):top()
	for k = 1, 3 do
		local p = pos:add(face:scale(k))

		-- End the rotating column at the first node that has
		-- "obsidian" in its description.  This allows players to
		-- choose to end the rotating column earlier.
		local def = p:nodedef()
		if def.name == "ignore" then return end
		if def == nil or def.name == "air"
			or def.description:contains("obsidian", true) then
			break
		end

		-- Check the "stats" for the node, and make sure it hasn't been
		-- rotated by another rotor too recently on a conflicting axis.  This
		-- means that 2 rotors trying to rotate the same node in opposite
		-- directions may conflict.
		local stats = p:meta():get_string("rotor_stats")
		if stats and stats ~= "" then
			stats = minetest.deserialize(stats)
			if stats.t > (now - rotor_interval)
				and not pos:eq(stats.pos)
				and face:dot(stats.axis) ~= 0 then
				pos:shatter("rotor conflict")
				return
			end
		end

		-- Rotate the current node, if it's rotatable.
		local hash = p:hash()
		local ndata = sz_table:new({ p = p, n = p:node_get() })
		oldnodes[hash] = ndata:serialize()
		if def.paramtype2 == "facedir" then
			ndata.n.param2 = sz_facedir:from_param(ndata.n.param2):rotate(face).param
		end
		newnodes[hash] = ndata:serialize()

		-- Check each direction to the side for wallmounts, like torches,
		-- that are attached to the rotating node.  If all wallmounts can
		-- find a place to go that is either open, or occupied by another
		-- wallmount that will also be rotated away, then we can continue,
		-- otherwise the entire mechanism jams.
		for k, v in pairs(sz_pos.dirs) do
			-- Ignore directions that are not perpendicular to the
			-- axis of rotation.
			if v:dot(face) == 0 then

				-- Make sure that the node in that direction is a wallmount
				-- node, and is attached to the rotating node in the center.
				local w = p:add(v)
				if nodestuckto(p, w) then

					-- Check the corner node between the source and destination
					-- and seize up rotation if it's blocked, even though
					-- a node will never actually "occupy" that space.
					local passante = w:add(face:cross(v))
					local passdef = passante:nodedef()
					if passdef.walkable then
						if passdef.name == "ignore" then return end
						jammed = true
						break
					end

					-- Check the destination.  It must be either empty/air,
					-- or contain another wallmount that meets its criteria
					-- for rotation, i.e. would be rotated away, to continue.
					-- Any wallmount that holds up the works jams the ENTIRE
					-- mechanism.
					local dest = p:add(face:cross(v))
					local destdef = dest:nodedef()
					if destdef and destdef.name ~= "air" and
						not nodestuckto(p, dest) then
						if destdef.name == "ignore" then return end
						jammed = true
						break
					end

					local whash = w:hash()
					local dhash = dest:hash()

					-- Save the old state of the source node, and the new
					-- state of the dest node.  Note that we need to support both
					-- wallmounted and "facedirmounted" types of "attached" nodes.
					local wdata = sz_table:new({ p = w, n = w:node_get() })
					oldnodes[whash] = wdata:serialize()
					local srcdef = w:nodedef()
					if srcdef.paramtype2 == "wallmounted" then
						wdata.n.param2 = face:cross(sz_pos
							:from_wallmounted(wdata.n.param2))
							:to_wallmounted()
					elseif srcdef.paramtype2 == "facedir" then
						wdata.n.param2 = sz_facedir:from_param(wdata.n
							.param2):rotate(face).param
					end
					wdata.p = dest
					newnodes[dhash] = wdata:serialize()

					-- If something isn't being rotated into this spot already,
					-- we'll need to rotate in some air so we don't duplicate nodes.
					if not newnodes[whash] then
						newnodes[whash] = minetest.serialize({ p = w })
					end
				end
			end
		end

		-- Early bail-out if jammed.
		if jammed then break end
	end

	-- If the mechanism is jammed, but was not spinning, then simply don't spin
	-- it.  If it was spinning, then the rotor fails catastrophically.
	if jammed then
	local spin = pos:meta():get_float("spinning")
		if spin and spin > 0 then
			pos:shatter("jammed while spinning")
		end
		return
	end

	-- Some "stats" about the current rotation, so that other rotors that try
	-- to rotate the same nodes know if there's a conflict.
	local stats = sz_table:new()
	stats.t = now
	stats.axis = face
	stats.pos = pos
	local statstr = stats:serialize()

	-- If the mechanism is not jammed, process all of the delayed commit actions.
	-- Only modify each node location once, and skip modifications for which the
	-- node itself wouldn't have changed, for efficiency, though we still process
	-- the "rotated" event on each node.
	local sounddone
	local finish = { }
	for k, v in pairs(newnodes) do
		-- Apply the updated node content.
		local t = minetest.deserialize(v)
		local p = sz_pos:new(t.p)
		if v ~= oldnodes[k] then
			if t.n then
				p:node_swap(t.n)
			else
				p:node_set()
			end
		end

		-- Mark some "rotor-rotated" stats for the node, to prevent another
		-- from rotating these nodes on the same axis too soon.
		p:meta():set_string("rotor_stats", statstr)

		-- Call the "rotor_rotated" event hook.
		local def = sz_pos:new(p):nodedef()
		if def.rotor_rotated then
			finish[#finish + 1] = function() def.rotor_rotated(t.p, pos, face) end
		end

		-- Make sound if we actually rotated something.
		if not sounddone then
			sounddone = pos:add(face):sound("sz_rotary_rotor")
		end
	end

	-- Keep track of the fact that the rotor is now "spinning" until it's powered
	-- down (which will result in replacing the node and thus removing the metadata).
	pos:meta():set_float("spinning", 1)

	-- Call all "rotated" callbacks after we finish committin our own node changes,
	-- as otherwise a crash in one of those methods could duplicate nodes.
	for k, v in pairs(finish) do v() end
end

local function rotor_power_check(pos, node)
	pos = sz_pos:new(pos)
	local pw, ign = pos:rotpower_in(sz_facedir:from_param(node.param2):bottom())
	if ign then return end
	if pw then
		if node.name ~= rot_pw_name then
			pos:node_set({
				name = rot_pw_name,
				param2 = node.param2
			})
		end
	else
		if node.name ~= rot_nopw_name then
			pos:node_set({
				name = rot_nopw_name,
				param2 = node.param2
			})
		end
	end
end

-- Rotors cannot rotate one another on non-perpendicular axes.
local function rotor_rotated_peer(pos, from, axis)
	pos = sz_pos:new(pos)
	local node = node or pos:node_get()
	local face = sz_facedir:from_param(node.param2):back()
	if face:dot(axis) ~= 0 then pos:shatter("rotated by other rotor") end
end

local function reg_rotor(name, def)
	return sz_rotary.register_node_facedir(name, sz_table.mergedeep(def or {}, {
		description = "Rotor",
		drop = rot_nopw_name,
		groups = {
			cracky = 1,
			sz_rotor = 1,
			sz_saw_delicate = 1
		},
		rotary_power_check = rotor_power_check,
		sounds = default.node_sound_stone_defaults(),
		rotor_rotated = rotor_rotated_peer
	}))
end

reg_rotor(rot_nopw_name, {
        tiles = {
                "sz_rotary_rotor_top.png",
		"sz_lib_rotary_stonebox_input.png",
		"sz_rotary_rotor_side.png"
        },
})
reg_rotor(rot_pw_name, {
        tiles = {
		sz_rotary.std_anim("sz_rotary_rotor_top_anim.png", rot_anim_time * 4),
		"sz_lib_rotary_stonebox_input.png",
		sz_rotary.std_anim("sz_rotary_rotor_side_anim.png", rot_anim_time)
        },
	on_timer = rotor_spin,
	on_construct = function(pos) sz_pos:new(pos):timer():start(rotor_interval) end,
	groups = {not_in_creative_inventory = 1, sz_rotary_ambiance = 1}
})

minetest.register_craft({
	output = rot_nopw_name,
	recipe = {
		{ "default:desert_stone", "default:desert_stone", "default:desert_stone" },
		{ "default:stone", "sz_rotary_transmit:gearbox", "default:stone" },
		{ "default:stone", "sz_lib_rotary:gear_wood", "default:stone" },
	}
})
