-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, rawget, rawset, table
    = minetest, pairs, rawget, rawset, table
local table_concat, table_sort
    = table.concat, table.sort
-- LUALOCALS > ---------------------------------------------------------

local function alertplayer(player) end
	
local fakemods = rawget(_G, "sz_fakemods")
if not fakemods then
	fakemods = {}
	rawset(_G, "sz_fakemods", fakemods)
	
	local fakemodlist = ""
	minetest.after(0, function()
		local x = {}
		for k, v in pairs(fakemods) do
			x[#x + 1] = v
		end
		table_sort(x)
		fakemodlist = table_concat(x, ", ")
	end)
	
	minetest.register_on_joinplayer(function(player)
		local name = player:get_player_name()
		if not minetest.get_player_privs(name).server then return end
		minetest.chat_send_player(name, 
			"The following are not real mods, and should be disabled: "
			.. fakemodlist)
	end)
end
fakemods[minetest.get_current_modname()] = true
