-- LUALOCALS < ---------------------------------------------------------
local ItemStack, default, ipairs, math, minetest, pairs, print,
      sz_facedir, sz_pos, sz_table, table, tostring, type
    = ItemStack, default, ipairs, math, minetest, pairs, print,
      sz_facedir, sz_pos, sz_table, table, tostring, type
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

------------------------------------------------------------------------
-- CRUCIBLE MELTING RECIPES

-- After everything has been loaded (we need to wait for all mods and
-- all recipes to be registered) find out what things melt to.
local meltsto
minetest.after(1, function()
	meltsto = sz_table:new()

	-- Determine the list of things that survive melting in a
	-- crucible.  Everything not on this list is burned up.
	for k, v in pairs(minetest.registered_items) do
		if k:endswith("ingot") then
			meltsto[k] = sz_table:new({ [k] = 1 })
		end
	end
	for k, v in pairs({
		"default:glass",
		"default:obsidian_glass",
		"default:stone",
		"default:desert_stone",
		}) do
		meltsto[v] = sz_table:new({ [v] = 1 })
	end

	local serfuncs = { }
	local function recipeser(tbl)
		local f = serfuncs[type(tbl)]
		if f then return f(tbl) end
		return tostring(tbl)
	end
	function serfuncs.userdata(t)
		return t:to_string()
	end
	function serfuncs.table(t)
		local tt = sz_table:new()
		local kz = sz_table.keys(t)
		for i, k in ipairs(kz) do
			tt:insert(recipeser(k))
			tt:insert(recipeser(t[k]))
		end
		return tt:concat("|")
	end

	-- Scan forwards cooking and reverse craftin recipes to find all items
	-- that contain any of the target items.
	local function scancrafts(item, seen)
		-- Group recipe items don't return anything.
		if item:startswith("group:") then return { } end

		-- Handle cached results.
		local found = meltsto[item]
		if found then return found end

		-- Create a copy of the seen data (for local modifications),
		-- track any results found, and provide a helper for returning
		-- those results if any found.
		seen = sz_table.copy(seen)
		local results = sz_table:new()
		local function done()
			meltsto[item] = results
			return results
		end
		local function addscan(i, m)
			m = m or 1
			for k, v in pairs(scancrafts(i, seen)) do
				results[k] = (results[k] or 0) + v * m
			end
		end

		-- First, look for a custom "meltsto" registration on the
		-- item, specifying its melting quantity for difficult-to-handle
		-- items like saw-cut ones.
		local reg = minetest.registered_items[item]
		if reg and reg.meltsto then
			for k, v in pairs(reg.meltsto) do
				addscan(k, v)
			end
			for ik, iv in pairs(results) do
				return done()
			end
		end
	
		-- Check the cooking recipe and accept if it include a target item.
		local cooked = minetest.get_craft_result({
			method = "cooking",
			width = 1,
			items = { item }
		})
		if cooked and cooked.item then
			local chk = recipeser(cooked)
			if not seen[chk] then
				seen[chk] = true
				local stack = ItemStack(cooked.item)
				if not stack:is_empty() then
					addscan(stack:get_name(), stack:get_count())
					return done()
				end
			end
		end

		-- Search all reverse crafting recipes and accept
		-- the first one that results in target items.
		local all = minetest.get_all_craft_recipes(item)
		if all then
			for k, v in pairs(all) do
				local chk = recipeser(v)
				if not seen[chk] and v.type == "normal" or v.type == "shapeless" then
					seen[chk] = true
					local div = ItemStack(v.output):get_count()
					for i, iv in pairs(v.items) do
						local stack = ItemStack(iv)
						addscan(stack:get_name(), stack:get_count() / div)
					end
					for ik, iv in pairs(results) do
						return done()
					end
				end
			end
		end

		return done()
	end
	for k, v in pairs(minetest.registered_items) do
		if k and k ~= "" then scancrafts(k, { }) end
	end

	-- Clean up the "meltsto" table and remove any items that
	-- melt to nothing.
	local nomelt = { }
	for k, v in pairs(meltsto) do
		local any = false
		for ik, iv in pairs(v) do
			any = true
			break
		end
		if not any then nomelt[k] = true end
	end
	for k, v in pairs(nomelt) do
		meltsto[k] = nil
	end
end)

------------------------------------------------------------------------
-- CRUCIBLE NODES

local empty_name = "sz_heat:crucible"
local full_name = "sz_heat:crucible_full"
local molten_name = "sz_heat:crucible_molten"
local residue_name = "sz_heat:crucible_residue"

local function reg(name, def, depth)
	depth = depth or -3/8
	return minetest.register_node(name, sz_table.mergedeep(def or { }, {
		description = "Crucible",
		drop = empty_name,
		drawtype = "nodebox",
		node_box = {
			type = "fixed",
			fixed = {
				{ -3/8, -0.5, -3/8, 3/8, depth, 3/8 },
				{ -0.5, -0.5, -0.5, 3/8, 0.5, -3/8 },
				{ 0.5, -0.5, -0.5, 3/8, 0.5, 3/8 },
				{ 0.5, -0.5, 0.5, -3/8, 0.5, 3/8 },
				{ -0.5, -0.5, 0.5, -3/8, 0.5, -3/8 }
			}
		},
		selection_box = {
			type = "fixed",
			fixed = { -0.5, -0.5, -0.5, 0.5, 0.5, 0.5 }
		},
		tiles = {
			"sz_heat_pyrosilica_glass.png",
			"sz_heat_pyrosilica_glass.png",
			"sz_heat_pyrosilica_glass.png",
			"sz_heat_pyrosilica_glass.png",
			"sz_heat_pyrosilica_glass.png",
			"sz_heat_pyrosilica_glass.png"
		},
		paramtype = "light",
		paramtype2 = "facedir",
		groups = {
			cracky = 1,
			level = 2,
			sz_crucible = 1
		},
		on_construct = function(pos)
			local inv = sz_pos:new(pos):meta():get_inventory()
			inv:set_size("main", 16)
		end,
		sounds = default.node_sound_glass_defaults()
	}))
end

reg(empty_name)
reg(full_name, {
	tiles = {
		"sz_heat_pyrosilica_glass.png^sz_heat_crucible_full.png"
	}
}, 0.25)
reg(molten_name, {
	tiles = {
		{
			name = "sz_heat_crucible_molten.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1
			}
		}
	},
	damage_per_second = 4
}, 0.25)
reg(residue_name, {
	tiles = {
		{
			name = "sz_heat_crucible_residue.png",
			animation = {
				type = "vertical_frames",
				aspect_w = 16,
				aspect_h = 16,
				length = 1
			}
		}
	},
	damage_per_second = 1
})

------------------------------------------------------------------------
-- CRUCIBLE ABM

minetest.register_abm({
	nodenames = { "group:sz_crucible" },
	interval = 1,
	chance = 1,
	action = function(pos)
		-- Make sure engine is intialized first; this may take a second
		-- after startup to ensure that all recipes and items are already
		-- loaded.
		if not meltsto then return end

		pos = sz_pos:new(pos)
		local meta = pos:meta()
		local inv = meta:get_inventory()

		-- Calculate how much molten stuff is in the crucible.
		local molten = meta:get_string("molten")
		molten = molten and minetest.deserialize(molten) or { }
		local moltenqty = 0
		for k, v in pairs(molten) do
			moltenqty = moltenqty + v
		end

		-- Pull in items dropped on/in the crucible, unless it's full
		-- of molten stuff.
		if moltenqty < 99 * 16 then
			local above = pos:add(sz_pos.dirs.u)
			local topface = pos:add(sz_pos.dirs.u:scale(0.5))
			for k, v in pairs(above:objects_in_radius(3)) do
				local npos = sz_pos.round(v:getpos())
				if not v:is_player() and (npos:eq(above) or npos:eq(pos)) then
					local ent = v:get_luaentity()
					if ent and ent.itemstring then
						local stack = ItemStack(inv:add_item("main", ent.itemstring))
						if stack:is_empty() then
							v:remove()
						else
							ent.itemstring = stack:to_string()
						end
					end
				end
			end
		end

		-- If the crucible is tipped over and there's space out the "top", then
		-- dump all relevant contents.
		local top = sz_facedir:from_param(pos:node_get().param2):top()
		if not top:eq(sz_pos.dirs.u) then
			local todump = { }
			local dump
			for k, v in pairs(molten) do
				if v >= 0 then
					v = math_floor(v)
					if v > 0 then
						todump[k] = v
						dump = true
					end
				end
			end

			local outpos = pos:add(top)
			if dump then
				local dumpfunc
				if outpos:is_empty() or outpos:fire_allowed() then
					outpos:node_set({ name = "sz_heat:slag_source" })
					dumpfunc = function(s)
						outpos:meta():get_inventory():add_item("main", s)
					end
				elseif outpos:groups().puts_out_fire then
					dumpfunc = function(s)
						outpos:item_eject(s)
					end
				end
				if dumpfunc then
					local o = dumpfunc
					dumpfunc = function(s) print(s) o(s) end
					for k, v in pairs(todump) do
						local stack = ItemStack(k)
						local max = stack:get_stack_max()
						while v > max do
							stack:set_count(max)
							dumpfunc(stack:to_string())
							v = v - max
							molten[k] = molten[k] - max
						end
						if v > 0 then
							stack:set_count(v)
							dumpfunc(stack:to_string())
							molten[k] = molten[k] - v
						end
					end
				end
			end
			for k, v in pairs(inv:get_list("main")) do
				outpos:item_eject(v)
			end
			inv:set_list("main", { })
		end

		-- If there are items in the crucible to melt, accumulate latent heat.
		local heat
		if inv:is_empty("main") then
			heat = 0
		else
			heat = meta:get_float("heat") or 0
			heat = heat + pos:heat_level() / 20
		end

		-- Melt down each item for which there is enough heat.
		while heat >= 1 do
			local chosen
			for i = 1, 16 do
				chosen = ItemStack(inv:get_stack("main", i))
				if chosen:is_empty() then
					chosen = nil
				else
					chosen = chosen:get_name()
					break
				end
			end
			if not chosen then break end
			local out = meltsto[chosen] or { }
			local t = 0
			for k, v in pairs(out) do
				t = t + v
			end
			if t < 1 then t = 1 end
			if heat < t then break end
			heat = heat - t
			inv:remove_item("main", chosen)
			for k, v in pairs(out) do
				molten[k] = (molten[k] or 0) + v
				moltenqty = moltenqty + v
			end
		end

		-- Determine appearance of crucible based on contents.
		local name
		if not inv:is_empty("main") then
			name = full_name
		elseif moltenqty <= 0 then
			name = empty_name
		else
			local ingot
			for k, v in pairs(molten) do
				if v >= 1 then
					ingot = true
					break
				end
			end
			if ingot then
				name = molten_name
			else
				name = residue_name
			end
		end
		local node = pos:node_get()
		if name ~= node.name then
			node.name = name
			pos:node_swap(node)
		end

		-- Save metadata.
		meta:set_float("heat", heat)
		meta:set_string("molten", minetest.serialize(molten))
	end
})
