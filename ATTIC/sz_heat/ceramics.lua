-- LUALOCALS < ---------------------------------------------------------
local default, math, minetest, stairs, sz_pos
    = default, math, minetest, stairs, sz_pos
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local rootsub = "pyrosilica"
local claysub = rootsub .. "_clay"
local clayname = modname .. ":" .. claysub
local glasssub = rootsub .. "_glass"
local glassname = modname .. ":" .. glasssub

minetest.register_node(clayname, {
	description = "Pyrosilica Clay",
	tiles = { modname .. "_pyrosilica_clay.png" },
	groups = {
		crumbly = 1,
		[modname .. "_" .. claysub] = 1,
	},
	sounds = default.node_sound_dirt_defaults()
})
stairs.register_stair_and_slab(claysub, clayname)

minetest.register_node(glassname, {
	description = "Pyrosilica Glass",
	tiles = { modname .. "_pyrosilica_glass.png" },
	groups = {
		cracky = 1,
		level = 2
	},
	sounds = default.node_sound_glass_defaults()
})
stairs.register_stair_and_slab(glasssub, glassname)

minetest.register_abm({
	nodenames = { "group:" .. modname .. "_" .. claysub },
	neighbors = { "group:hot" },
	interval = 5,
	chance = 1,
	action = function(pos, node)
		pos = sz_pos:new(pos)

		-- Must be at an extremely high temperature to bake.
		if pos:heat_level() < 30 then return end

		-- Figure out what kind of node we're cooking this one
		-- into and validate that it's registered.
		local name = pos:node_get().name:gsub("_clay", "_glass")
		if not minetest.registered_nodes[name] then return end

		-- Cooking item causes organic particles from the clay
		-- to flake off, creating a cloud of dust that ignites
		-- easily, and makes manual kiln operation dangerous.
		pos:scan_flood(5, function(p)
			if p:eq(pos) then return end
			if not p:fire_allowed() then return false end
			if math_random(1, 5) == 1 then
				p:node_set({ name = "fire:basic_flame" })
			end
		end)
		
		-- Item must be baked for a certain amount of time
		-- total to be converted.
		local meta = pos:meta()
		local bake = meta:get_float("bake") or 0
		bake = bake + 1
		if bake < 5 then
			meta:set_float("bake", bake)
			return
		end

		-- Convert item to its molten glass equivalent.
		pos:node_set({ name = modname .. ":slag_source" })
		pos:meta():get_inventory():add_item("main", name)
	end
})
