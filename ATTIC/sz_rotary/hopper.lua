-- LUALOCALS < ---------------------------------------------------------
local ItemStack, default, minetest, pairs, sz_pos, sz_rotary
    = ItemStack, default, minetest, pairs, sz_pos, sz_rotary
-- LUALOCALS > ---------------------------------------------------------

------------------------------------------------------------------------
-- WATER PUMP

local hopp_pw_name = "sz_rotary:hopper_powered"
local hopp_nopw_name = "sz_rotary:hopper"

local function hopp_power_check(pos, node)
	pos = sz_pos:new(pos)
	local pw, ign
	for k, v in pairs({ sz_pos.dirs.n, sz_pos.dirs.s, sz_pos.dirs.e, sz_pos.dirs.w }) do
		pw, ign = pos:rotpower_in(v)
		if ign then return end
		if pw then break end
	end
	if not pw then
		if node.name ~= hopp_nopw_name then
			pos:node_set({ name = hopp_nopw_name })
		end
		return
	else
		if node.name ~= hopp_pw_name then
			pos:node_set({ name = hopp_pw_name })
		end
	end
end

local function reg_hopp(name, def)
	def.description = def.description or "Hopper"
	def.drop = def.drop or hopp_nopw_name
	def.groups = def.groups or { }
	def.groups.choppy = def.groups.choppy or 2
	def.groups.oddly_breakable_by_hand = def.groups.oddly_breakable_by_hand or 1
	def.groups.sz_hopper = def.groups.sz_hopper or 1
	def.groups.flammable = def.groups.flammable or 1
	def.groups.saw_delicate = def.groups.saw_delicate or 1
	def.rotary_power_check = def.rotary_power_check or hopp_power_check
	def.sounds = def.sounds or default.node_sound_wood_defaults()
	return sz_rotary.register_node(name, def)
end

reg_hopp(hopp_nopw_name, {
	tiles = {
		"sz_rotary_hopper_top_anim.png^[verticalframe:16:1",
		"sz_rotary_hopper_top_anim.png^[transformR180^[verticalframe:16:1",
		"sz_rotary_woodbox_input.png"
	}
})
reg_hopp(hopp_pw_name, {
	tiles = {
		sz_rotary.std_anim("sz_rotary_hopper_top_anim.png"),
		sz_rotary.std_anim("sz_rotary_hopper_top_anim.png^[transformR180"),
		"sz_rotary_woodbox_input.png"
	}
})

minetest.register_abm({
	nodenames = { hopp_pw_name },
	interval = 1,
	chance = 1,
	action = function(pos)
		pos = sz_pos:new(pos)

		local consume
		local below = pos:add(sz_pos.dirs.d)
		if below:node_get().name == "ignore" then return end
		if below:is_empty() then
			consume = function(stack)
				below:item_eject(stack)
				return ""
			end
		else
			local inv = below:meta():get_inventory()
			for k, v in pairs({ "main", "src" }) do
				local size = inv:get_size(v)
				if size and size > 0 then
					consume = function(stack)
						return inv:add_item(v, stack)
					end
					break
				end
			end
		end
		if not consume then return end

		local above = pos:add(sz_pos.dirs.u)
		for k, v in pairs(above:objects_in_radius(2)) do
			if not v:is_player() and sz_pos.round(v:getpos()):eq(above) then
				local ent = v:get_luaentity()
				if ent and ent.itemstring then
					local stack = ItemStack(consume(ent.itemstring))
					if stack:is_empty() then
						v:remove()
					else
						ent.itemstring = stack:to_string()
					end
				end
			end
		end

		if above:groups().items_on_ground then
			local inv = above:meta():get_inventory()
			local t = inv:get_list("main")
			for i, v in pairs(t) do
				t[i] = consume(v)
			end
			inv:set_list("main", t)
		end
	end
})
