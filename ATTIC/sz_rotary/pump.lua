-- LUALOCALS < ---------------------------------------------------------
local default, ipairs, math, minetest, pairs, print, sz_pos, sz_rotary,
      sz_table
    = default, ipairs, math, minetest, pairs, print, sz_pos, sz_rotary,
      sz_table
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

------------------------------------------------------------------------
-- WATER PUMP

local pump_pw_name = "sz_rotary:waterpump_powered"
local pump_nopw_name = "sz_rotary:waterpump"

local water_source = "default:water_source"
local water_flowing = "default:water_flowing"

local pump_input_dirs = {
	sz_pos.dirs.n,
	sz_pos.dirs.s,
	sz_pos.dirs.e,
	sz_pos.dirs.w,
}

local pump_solid_drawtypes = {
	normal = true,
	glasslike = true,
	glasslike_framed = true,
	allfaces = true,
	allfaces_optional = true,
}

-- Function to check pump operation and create/remove the water
-- source block above.  Also called when the pump is removed,
-- to automatically remove the water source block.
local function pump_water_check(pos)
	pos = sz_pos:new(pos)
	local above = pos:add(sz_pos.dirs.u)

	-- Make sure the pump is still present and powered.
	local pnode = pos:node_get()
	local valid = pnode and pnode.name == pump_pw_name

	-- Pumps that are forced to "stall" due to a pump loop
	-- cannot work again until the stall time limit passes.
	if valid then
		local t = pos:meta():get_float("stall") or 0
		valid = t < minetest.get_gametime()
	end
	
	-- Check each input side.
	if valid then
		local wet = false
		local vaporlock = false

		for k, v in ipairs(pump_input_dirs) do
			local p = pos:add(v)

			-- Check for deep-enough water.  A source block or deep
			-- enough flowing water is OK; too-shallow flowing water
			-- will let air into the pump.
			if p:groups().water then
				if p:fluid_depth() > 3 then
					wet = true
				else
					vaporlock = true
				end
			else
				-- Make sure that any side that does not have water has a solid
				-- block to stop air intake, otherwise the pump will just suck
				-- in air and not move water.
				local pdef = p:nodedef()
				if not pdef or not pdef.walkable or pdef.climbable
					or not pump_solid_drawtypes[pdef.drawtype] then
					vaporlock = true
				end
			end
		end

		valid = valid and wet and not vaporlock
	end

	-- Do not enable pump output if there is a water source
	-- block in the vicinity of the pump output, to reduce the
	-- risk this is used exploitively to create an infinite
	-- spring, thus allowing the pump to be removed.
	for k, v in pairs(minetest.find_nodes_in_area(above:add(sz_pos:xyz(-2, 0, -2)),
		above:add(sz_pos:xyz(2, 0, 2)), { water_source })) do
		if not above:eq(v) then
			valid = false
			break
		end
	end

	-- Add or remove the water source block as necessary based on pump valid state.
	local anode = above:node_get()
	if valid then
		if not anode or anode.name == "air" or anode.name == water_flowing then
			above:node_set({ name = water_source })
		end
	else
		if anode.name == water_source then
			above:node_set({ name = water_flowing, param2 = 7 })
		end
	end
end

local function pump_power_check(pos, node)
	pos = sz_pos:new(pos)
	local pw, ign = pos:rotpower_in(sz_pos.dirs.d)
	if ign then return end
	if not pw then
		if node.name ~= pump_nopw_name then
			pos:node_set({ name = pump_nopw_name })
			pump_water_check(pos)
		end
		return
	else
		if node.name ~= pump_pw_name then
			pos:node_set({ name = pump_pw_name })
			pump_water_check(pos)
		end
	end
end

local function reg_pump(name, def)
	def.description = def.description or "Water Pump"
	def.drop = def.drop or pump_nopw_name
	def.groups = def.groups or { }
	def.groups.cracky = def.groups.cracky or 1
	def.groups.sz_waterpump = def.groups.sz_waterpump or 1
	def.groups.saw_delicate = def.groups.saw_delicate or 1
	def.rotary_power_check = def.rotary_power_check or pump_power_check
	def.sounds = def.sounds or default.node_sound_stone_defaults()
	def.after_destruct = pump_water_check
	return sz_rotary.register_node(name, def)
end

reg_pump(pump_nopw_name, {
	tiles = {
		"sz_rotary_pump_output.png",
		"sz_rotary_stonebox_input.png",
		"sz_rotary_stonebox_water_input.png",
		"sz_rotary_stonebox_water_input.png",
		"sz_rotary_stonebox_water_input.png",
		"sz_rotary_stonebox_water_input.png",
	},
})
reg_pump(pump_pw_name, {
	tiles = {
		sz_rotary.std_anim("sz_rotary_pump_output_anim.png", 1),
		"sz_rotary_stonebox_input.png",
		"sz_rotary_stonebox_water_input.png",
		"sz_rotary_stonebox_water_input.png",
		"sz_rotary_stonebox_water_input.png",
		"sz_rotary_stonebox_water_input.png",
	},
})

minetest.register_abm({
	nodenames = { pump_pw_name },
	interval = 5,
	chance = 1,
	action = pump_water_check
})

-- Pumps may be exploitable by carefully setting up a continuous water loop,
-- since they only check their inputs infrequently, and the source of
-- water could be replaced between checks.  This could allow someone to
-- run a series of pumps to a remote location, setup a loop there, then
-- remove the original pump series from source to use elsewhere.  Check
-- periodically for pump loops.  The check is expensive, so do it rarely.
-- If we detect a pump loop, break one of the pumps, to make the consequences
-- expensive as well.

local pump_loop_trace_dirs = sz_table:new({
	sz_pos.dirs.n,
	sz_pos.dirs.s,
	sz_pos.dirs.e,
	sz_pos.dirs.w,
	sz_pos.dirs.d,
})

local function pump_loop_check(pos, start, seen)

	-- Stop if we have hit the trace node limit.
	if seen.limit < 1 then return end

	-- Ignore paths we've already traced.
	local hash = pos:hash()
	if seen[hash] then return end

	-- Add this path to the trace count, and deduct
	-- from our path limit.
	seen[hash] = true
	seen.limit = seen.limit - 1

	-- If we reach the start again, a loop has been found.
	if pos:eq(start) then
		print("pump loop found at " .. pos:to_string())
		return true
	end

	-- If the node is a powered pump with a water source above it,
	-- then trace upwards through the pump, so we can't be fooled by
	-- a loop containing more than one pump.
	local node = pos:node_get()
	if node and node.name == pump_pw_name then
		seen.limit = seen.limit - 1
		pos = pos:add(sz_pos.dirs.u)
		node = pos:node_get()
		if not node or node.name ~= water_source then
			return
		end

	-- If the node is not a powered pump, it needs to be a water
	-- node OTHER than a water source for us to trace through.
	elseif not node or node.name == water_source
		or not pos:groups().water then
		return
	end

	-- Trace each node around the current one.  Traverse in a
	-- random order to avoid following a predictable path.
	pump_loop_trace_dirs:shuffle()
	for i, v in ipairs(pump_loop_trace_dirs:copy()) do
		local found = pump_loop_check(pos:add(v), start, seen)

		-- If a loop was found, then "stall" random pumps along its route,
		-- by draining the output water.  This should be enough to break
		-- the loop.
		if found then
			if node.name == water_source and math_random(1, 4) ~= 1 then
				pos:node_set({ name = water_flowing, param2 = 7 })

				-- Prevent the pump from re-energizing for a random amount
				-- of time.  The "stall time" makes sure that the loop is
				-- properly killed and downstream pumps get a chance to dry
				-- up, and the random time ensures that creating a "pulsed"
				-- pump is not feasible.
				local t = 20
				while math_random(1, 2) == 1 do t = t * 2 end
				local ppos = pos:add(sz_pos.dirs.d)
				print("pump at " .. ppos:to_string() .. " stalled for "
					.. t .. " seconds")
				ppos:meta():set_float("stall",
					minetest.get_gametime() + t)
			end
			return found
		end
	end
end

minetest.register_abm({
	nodenames = { pump_pw_name },
	interval = 1,
	chance = 10,
	action = function(pos)
		local pos = sz_pos:new(pos)

		local above = pos:add(sz_pos.dirs.u)
		local anode = above:node_get()
		if not anode or anode.name ~= water_source then return end

		-- Choose an exponetially-random-distributed search depth
		local seen = sz_table:new()
		seen.limit = 64
		for x = 1, 6 do
			if math_random(1, 2) == 1 then break end
			seen.limit = seen.limit * 2
		end

		local found = pump_loop_check(above:add(sz_pos.dirs.n), pos, seen)
			or pump_loop_check(above:add(sz_pos.dirs.s), pos, seen)
			or pump_loop_check(above:add(sz_pos.dirs.e), pos, seen)
			or pump_loop_check(above:add(sz_pos.dirs.w), pos, seen)
	end
})

