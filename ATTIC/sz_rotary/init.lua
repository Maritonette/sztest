-- LUALOCALS < ---------------------------------------------------------
local sz_class
    = sz_class
-- LUALOCALS > ---------------------------------------------------------

sz_class:loadsubclasses(
	"sz_rotpower",
	"sz_rotary")

sz_class:loadlibs(
	"transmission",
	"clutch",
	"turnstile",
	"waterturbine",
	"pump",
	"hopper",
	"rotor",
	"sawblade",
	"crafting")
