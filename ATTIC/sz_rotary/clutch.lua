-- LUALOCALS < ---------------------------------------------------------
local default, minetest, pairs, sz_facedir, sz_pos, sz_rotary, type
    = default, minetest, pairs, sz_facedir, sz_pos, sz_rotary, type
-- LUALOCALS > ---------------------------------------------------------

------------------------------------------------------------------------
-- WATER PUMP

local on_name = "sz_rotary:clutch"
local off_name = "sz_rotary:cluch_disengaged"

local gb_dis = "sz_rotary:gearbox_disabled"

local function clutch_check(forceon, pos)
	pos = sz_pos:new(pos)

	local pn = pos:node_get()
	local facedir = sz_facedir:from_param(pn.param2)
	local below = pos:add(facedir:bottom())

	local off = not forceon and pn.name == off_name

	local n = below:node_get()
	if off and below:groups().sz_gearbox and n.name ~= gb_dis then
		n.name = gb_dis
	elseif not off and n.name == gb_dis then
		for k, v in pairs(sz_pos.dirs) do
			local p = below:add(v)
			if not p:eq(pos) then
				local ppn = p:node_get()
				if ppn.name == off_name and
					sz_facedir:from_param(p:node_get().param2):top():eq(v) then
					off = true
					break
				end
			end
		end
		if not off then
			n.name = "sz_rotary:gearbox"
		end
	end
	below:node_swap(n)
	below:rotary_power_check()
end

local tt_cache = { }
local function clutch_toggle(pos)
	pos = sz_pos:new(pos)

	local now = minetest.get_gametime()
	local cacheidx = pos:hash()
	local tt = tt_cache[cacheidx] or pos:meta():get_float("toggletime")
	if (tt or 0) > (now - 2) then return end

	local n = pos:node_get()
	if n.name == on_name then
		n.name = off_name
	else
		n.name = on_name
	end

	pos:node_set(n)
	tt_cache[cacheidx] = now
	pos:meta():set_float("toggletime", now)
end

local function reg_clutch(name, def)
	def.description = def.description or "Clutch Lever"
	def.drawtype = def.drawtype or "nodebox"
	def.paramtype = def.paramtype or "light"
	def.paramtype2 = def.paramtype2 or "facedir"
	def.drop = def.drop or on_name
	def.groups = def.groups or { }
	def.groups.cracky = def.groups.cracky or 3
	def.groups.sz_clutch = def.groups.sz_clutch or 1
	def.groups.saw_delicate = def.groups.saw_delicate or 1
	def.sounds = def.sounds or default.node_sound_stone_defaults()
	def.on_destruct = def.on_destruct or function(...) return clutch_check(true, ...) end
	def.on_construct = def.on_construct or function(...) return clutch_check(nil, ...) end
	def.on_rightclick = def.on_rightclick or clutch_toggle
	def.on_place = minetest.rotate_and_place
	def.selection_box = def.selection_box or {
		type = "fixed",
		fixed = { -7/16, -0.5, -7/16, 7/16, -0.125, 7/16 }
	}
	return sz_rotary.register_node(name, def)
end

reg_clutch(on_name, {
	node_box = {
		type = "fixed",
		fixed = {
			{ -7/16, -0.5, -7/16, 7/16, -0.25, 7/16 },
			{ -3/16, -0.25, -3/16, 3/16, -0.125, 3/16 },
			{ -0.125, -0.25, 3/16, 0.125, -0.125, 7/16 }
		}
	},
	tiles = {
		"default_cobble.png^sz_rotary_clutch_on.png",
		"default_cobble.png",
		"default_steel_block.png^[lowpart:25:default_cobble.png"
	},
})

reg_clutch(off_name, {
	node_box = {
		type = "fixed",
		fixed = {
			{ -7/16, -0.5, -7/16, 7/16, -0.25, 7/16 },
			{ -3/16, -0.25, -3/16, 3/16, -0.125, 3/16 },
			{ 3/16, -0.25, -0.125, 7/16, -0.125, 0.125 }
		}
	},
	tiles = {
		"default_cobble.png^sz_rotary_clutch_off.png",
		"default_cobble.png",
		"default_steel_block.png^[lowpart:25:default_cobble.png"
	},
})
