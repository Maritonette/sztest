#!/usr/bin/perl -w
use strict;
use warnings;

my $w = 16;
my $h = $w;
my $frames = 16;
my $rot = -45;

my($src, $dst) = @ARGV;
$dst or die('Usage: ' . $0 . ' <source.png> <dest.png>');

sub mysys
	{
	print join(' ', map { '"' . $_ . '"' } @_) . $/;
	my $r = system(@_);
	$r == 0 or die ('command returned ' . $r);
	}

my @conv = ('convert', '-background', 'transparent', '-virtual-pixel', 'transparent');
my %tmps = ( );
eval
	{
	my @c = (@conv, '-page', $w . 'x' . ($h * $frames));
	for my $f ( 0 .. ($frames - 1) )
		{
		my $t = 'tmp-frame-' . $f . '.png';
		mysys(@conv, '-crop', $w . 'x' . $h . '+0+0!',
			'-distort', 'SRT', $rot * ($f / $frames), $src, $t);
		$tmps{$t} = 1;
		
		push @c, '-page', '+0+' . ($h * $f), $t;
		}
	push @c, '-layers', 'flatten', $dst;
	mysys(@c);
	};
for my $t ( keys %tmps )
	{
	unlink($t);
	}
$@ and die($@);
