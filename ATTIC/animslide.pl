#!/usr/bin/perl -w
use strict;
use warnings;

my $w = 16;
my $h = $w;
my $frames = 16;

my $src = 'source.png';
my $dst = 'dest.png';

sub mysys
	{
	print join(' ', map { '"' . $_ . '"' } @_) . $/;
	my $r = system(@_);
	$r == 0 or die ('command returned ' . $r);
	}

my @c = ('convert', '-background', 'transparent', '-virtual-pixel', 'transparent', '-page', $w . 'x' . ($h * $frames));
push @c, map
	{
	my $f = $_;
	my @d = map { s#^\+-#-#; $_; }
		map { ('-page', '+' . ((-$f / $frames + $_) * $w)
		. '+' . ($h * $f), $src ) } (0 .. 1);
	@d;
	} 0 .. ($frames - 1);
push @c, '-layers', 'flatten', $dst;
mysys(@c);
