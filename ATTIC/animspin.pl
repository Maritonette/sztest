#!/usr/bin/perl -w
use strict;
use warnings;

my $w = 16;
my $h = $w;
my $frames = 64;
my $rot = 360;

my $src = 'source.png';
my $dst = 'dest.png';

sub mysys
	{
	print join(' ', map { '"' . $_ . '"' } @_) . $/;
	my $r = system(@_);
	$r == 0 or die ('command returned ' . $r);
	}

my %tmps = ( );
eval
	{
	my @c = ('convert', '-page', ($w * 3) . 'x' . ($h * 3));
	push @c, map
		{
		my $x = $_ * $w;
		map
			{
			my $y = $_ * $h;
			( '-page', '+' . $x . '+' . $y, $src );
			} 0 .. 2
		} 0 .. 2;
	push @c, '-layers', 'flatten', 'tmp-tiled.png';
	mysys(@c);
	$tmps{'tmp-tiled.png'} = 1;

	@c = ('convert', '-page', $w . 'x' . ($h * $frames));
	for my $f ( 0 .. ($frames - 1) )
		{
		my $t = 'tmp-frame-' . $f . '.png';
		mysys('convert', '-distort', 'SRT', ($f / $frames * $rot), 'tmp-tiled.png',
			'-gravity', 'Center', '-crop', $w . 'x' . $h . '+0+0!', $t);
		$tmps{$t} = 1;
		
		push @c, '-page', '+0+' . ($h * $f), $t;
		}
	push @c, '-layers', 'flatten', $dst;
	mysys(@c);
	};
for my $t ( keys %tmps )
	{
	unlink($t);
	}
$@ and die($@);
