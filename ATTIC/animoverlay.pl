#!/usr/bin/perl -w
use strict;
use warnings;

my $w = 16;
my $h = $w;
my $frames = 16;

my($src, $ovl, $dst) = @ARGV;
$dst or die('Usage: ' . $0 . ' <source.png> <overlay.png> <dest.png>');

sub mysys
	{
	print join(' ', map { '"' . $_ . '"' } @_) . $/;
	my $r = system(@_);
	$r == 0 or die ('command returned ' . $r);
	}

my @c = ('convert', '-page', '+0+0', $src);
push @c, map { ('-page', '+0+' . ($h * $_), $ovl ); } 0 .. ($frames - 1);
push @c, '-layers', 'flatten', $dst;
mysys(@c);
