-- LUALOCALS < ---------------------------------------------------------
local default, minetest, pairs, print, sz_facedir, sz_pos, sz_rotary,
      sz_table, type
    = default, minetest, pairs, print, sz_facedir, sz_pos, sz_rotary,
      sz_table, type
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

-- Helper function to handle saw "cutting" at a position.
-- Depending on the type of motion (more may be supported in the
-- future), different nodes may be cut.
local function saw_cutnode(sawpos, actpos, moveddir)
	sawpos = sz_pos:new(sawpos)

	-- Validate that there's really a saw there
	local node = sawpos:node_get()
	if not node or not sawpos:groups().sz_sawblade then return end

	-- Any entities that get too close to the blade will get nicked.
	-- This searches a truncated spheroid along the blade's travel.
	local facedir = sz_facedir:from_param(node.param2)
	local topdir = facedir:top()
	sawpos:hitradius(2, 8, sz_pos:xyz(1, 1, 1):add(facedir:left():abs()))

	-- Check to see if there's a solid block in our "active" space.
	-- We don't operate on anything that's not solid.
	local actdef = actpos:nodedef()
	if not actdef or not actdef.walkable then return end

	-- If were rotating in the "bad" direction, i.e. swinging the flat
	-- of the blade, then we always break.
	if topdir:dot(moveddir) ~= 0 then
		sawpos:shatter("broadsided")
		return
	end

	-- Figure out the "level" of the sawblade, and of the thing being
	-- sawed.  Saws get a bonus against snappy, choppy, or crumbly,
	-- normal against cracky, and cannot cut anything else.  Anything
	-- cracky called "glass" incurs a penalty.
	local sawlv = sawpos:groups().level or 1
	local cutlv = actdef.groups.level or 1
	if actdef.groups.snappy or actdef.groups.choppy
		or actdef.groups.crumbly then
		cutlv = cutlv - 1
	elseif not actdef.groups.cracky then
		cutlv = 1000
	elseif actdef.description and actdef.description:gsub("Glass", "")
		~= actdef.description then
		cutlv = cutlv + 1
	end

	-- Saws cannot cut materials that are as hard as or
	-- harder than themselves.
	if cutlv >= sawlv then
		sawpos:shatter("material too hard " .. cutlv .. " >= " .. sawlv)
		return
	end

	-- Play sound and do particle effects.
	minetest.sound_play("sz_sawblade_cut", {
		pos = sawpos,
		gain = 4,
		max_hear_distance = 16
	})
	local sawctr = sawpos:add(facedir:front():scale(3/8))
	local sawdim = facedir:left():scale(0.5):add(facedir:top():scale(1/16))
	local sawdir = facedir:front():cross(moveddir:cross(facedir:front()))
	sawpos:limitfx("particle:sawdust", 5, 1, function()
		minetest.add_particlespawner({
			amount = 16,
			time = 0.25,
			minpos = sawctr:add(sawdim),
			maxpos = sawctr:sub(sawdim),
			minvel = sawdir:add(sz_pos:xyz(1, 1, 1)),
			maxvel = sawdir:scale(10):add(sz_pos:xyz(-1, -1, -1)),
			minacc = sz_pos.dirs.d:scale(10),
			maxacc = sz_pos.dirs.d:scale(10),
			minsize = 0.5,
			maxsize = 1,
			texture = actdef.tiles[4] or actdef.tiles[#actdef.tiles],
			collisiondetection = true
		})
	end)

	-- Nodes must be cut a few times to cut through completely.
	local needcuts = 4 - sawlv + cutlv
	if needcuts > 1 then
		local cutcount = actpos:meta():get_float("cutcount") or 0
		cutcount = cutcount + 1
		if cutcount < needcuts then
			actpos:meta():set_float("cutcount", cutcount)
			return
		end
	end

	-- Nodes may be marked with a simple group "saw_delicate" to
	-- indicate they're too delicate to be cut with a saw, and
	-- shatter instead.
	if actdef.groups and actdef.groups.sz_saw_delicate then
		actpos:shatter("shattered by sawblade")
	else
		-- Get the drops the block should generate when cut.
		local sawdrops = actdef.sawdrops
		if not sawdrops and actdef.drop and actdef.drop ~= "" then
			sawdrops = { [actdef.drop] = 1 }
		end
		if not sawdrops then
			sawdrops = { [actpos:node_get().name] = 1 }
		end

		-- Remove the node and eject its drops.
		print("sawblade cut " .. actdef.name .. " "
			.. minetest.serialize(sawdrops))
		actpos:node_set()
		for k, v in pairs(sawdrops) do
			actpos:item_eject(k, 1, v)
		end
	end
end

-- Handle saw being activated in rotary configuration.
local function saw_rotated(pos, rotorpos, axis)
	local facedir = sz_facedir:from_param(pos:node_get().param2)

	-- Calculate the direction of "movement", needed for checking
	-- if the blade shatters due to cutting on the wrong axis.
	local moveddir = rotorpos:sub(pos):cross(axis)
	if moveddir:len() == 0 then
		-- If we're on the axis of rotation, then we're being
		-- rotated at the tip, not revolving around the outside
		-- of the rotor column.  In this case, twisting the
		-- blade is the same as broadsiding it.
		moveddir = facedir:top()
	else
		moveddir = axis:cross(moveddir):norm():add(moveddir:norm())
	end
			
	-- Rotary saw blades cut the node the blade is pointing at.
	local actpos = pos:add(sz_facedir:from_param(pos:node_get().param2):front())
	saw_cutnode(pos, actpos, moveddir)
end

local function reg_sawblade(name, desc, groups, tile, def)
	local subname = name
	name = modname .. ":sawblade_" .. name

	minetest.register_craft({
		output = name,
		recipe = {
			{ "default:sword_" .. subname,
				"default:sword_" .. subname,
				"default:sword_" .. subname },
		}
	})

	def = def or { }

	local t = def.tiles or { tile }
	while #t < 6 do
		t[#t + 1] = t[#t]
	end
	def.tiles = t

	t[1] = t[1] .. "^sz_sawblade_top.png"
	t[2] = t[2] .. "^sz_sawblade_top.png^[transformFY"
	t[6] = t[6] .. "^sz_sawblade_edge.png"

        return sz_rotary.register_node_facedir(name, sz_table.mergedeep(def, {
		description = desc .. " Sawblade",
		drop = name,
		drawtype = "nodebox",
		node_box = {
			type = "fixed",
			fixed = { -0.5, -1/16, -0.25, 0.5, 1/16, 0.5 }
		},
		groups = sz_table.merge(groups, {
			cracky = 1,
			sz_sawblade = 1,
			can_shatter = 1,
			facedirmounted = 1
		}),
		paramtype = "light",
		paramtype2 = "facedir",
		sounds = default.node_sound_stone_defaults(),
		rotor_rotated = saw_rotated
	}))
end

reg_sawblade("stone", "Stone", { cracky = 3 }, "default_stone.png")
reg_sawblade("steel", "Steel", { level = 2 }, "default_steel_block.png")
reg_sawblade("bronze", "Bronze", { level = 2 }, "default_bronze_block.png")
reg_sawblade("mese", "Mese", { level = 3 }, "default_mese_block.png")
reg_sawblade("diamond", "Diamond", { level = 3 }, "default_diamond_block.png")
