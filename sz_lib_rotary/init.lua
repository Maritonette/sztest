-- LUALOCALS < ---------------------------------------------------------
local minetest, sz_class
    = minetest, sz_class
-- LUALOCALS > ---------------------------------------------------------

sz_class:loadsubclasses(
	"sz_rotpower",
	"sz_rotary"
)

sz_class:loadlibs(
	"ambiance",
	"powercheck"
)

------------------------------------------------------------------------
-- CRAFT ITEMS

minetest.register_craftitem("sz_lib_rotary:gear_wood", {
	description = "Wooden Gear",
	inventory_image = "sz_lib_rotary_gear_wood.png"
})

minetest.register_craft({
	output = "sz_lib_rotary:gear_wood 4",
	recipe = {
		{ "", "group:stick", "" },
		{ "group:stick", "group:wood", "group:stick" },
		{ "", "group:stick", "" },
	}
})
