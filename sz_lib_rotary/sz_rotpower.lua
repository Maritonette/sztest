-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, sz_pos, tostring
    = minetest, pairs, sz_pos, tostring
-- LUALOCALS > ---------------------------------------------------------

-- This class specifically represents the rotary power metadata being
-- output by a rotary node.

------------------------------------------------------------------------
-- CONSTRUCTORS AND STATIC PROPERTIES

local rotpower_key = "sz_rotary_power"

-- Extension of sz_pos: load the rotary power being output by
-- the node at this position.  This will always return an object.
function sz_pos:rotpower_out()
	if self:node_get().name == "ignore" then
		return sz_rotpower:new({ pos = self, ignore = true })
	end
	local m = self:meta()
	if m == nil then
		return sz_rotpower:new({ pos = self })
	end
	local pd = m:get_string(rotpower_key)
	if pd == nil then
		return sz_rotpower:new({ pos = self })
	end
	local t = minetest.deserialize(pd) or { }
	t.pos = self
	return sz_rotpower:new(t)
end

-- Extension of sz_pos: get the rotary power being supplied to this
-- position from the given direction (vector).  Only accepts "axle"
-- type power, unless anypower is true.  Returns nil if no power is
-- being supplied from there to here. 
function sz_pos:rotpower_in(pos, anypower)
	local p = self:add(pos)
	local ignore = p:node_get().name == "ignore"
	if ignore then return nil, true end
	local pw = p:rotpower_out()
	if pw and (anypower or pw.axle) and pw:dir_get(pos:neg()) then
		return pw
	end
end

------------------------------------------------------------------------
-- ARITHMETIC

-- Determine if two rotpower metadata values have the same contents.
function sz_rotpower:eq(rot)
	if self.qty ~= rot.qty
		or (not self.axle) ~= (not rot.axle)
		or not self.pos:eq(rot.pos) then
		return false
	end
	for k, v in pairs(sz_pos.dirs) do
		if self:dir_get(v) ~= rot:dir_get(v) then
			return false
		end
	end
	return true
end

------------------------------------------------------------------------
-- DIRECTION METHODS

-- Get whether or not power is being transmitted in a particular
-- direction (vector).
function sz_rotpower:dir_get(pos)
	pos = sz_pos:new(pos)
	local w = pos:to_wallmounted()
	if w == nil then return end
	return self.dirs and self.dirs[w]
end

-- Set whether or not power is being transmitted in a particular
-- direction (vector).  value will be interpreted as a boolean.
function sz_rotpower:dir_set(pos, value)
	pos = sz_pos:new(pos)
	local w = pos:to_wallmounted()
	if w == nil then return self end
	local dirs = self.dirs or { }
	self.dirs = dirs
	dirs[w] = value == true and 1 or nil
	return self
end

-- Clear all power output.
function sz_rotpower:clear()
	self.dirs = nil
	self.axle = nil
	self.qty = nil
	return self
end

------------------------------------------------------------------------
-- METADATA ACCESS METHODS

-- Write back to metadata.  pos is an optional new save location.
function sz_rotpower:save(pos)
	pos = pos and sz_pos:new(pos) or self.pos
	local m = pos:meta()
	if m == nil then return self end

	-- Don't save if nothing has changed.  Even though this is a
	-- relatively expensive check, it prevents us from getting in an
	-- infinite loop when power changes trigger checks that result
	-- in unconditional power recalcs and saves.
	if self:eq(pos:rotpower_out()) then return end

	-- Always trigger neighbor power checks, and return self, at
	-- the end of this method.
	local function tail()
		for k, v in pairs(sz_pos.dirs) do
			pos:add(v):rotary_power_check()
		end
		return self
	end

	-- Only actually save the table to metadata if it's valid, i.e.
	-- has a non-zero quantity being output to at least 1 direction.
	if self.qty and self.qty > 0 and self.dirs then
		for k, v in pairs(self.dirs) do
			m:set_string(rotpower_key, minetest.serialize({
				axle = self.axle == true and 1 or nil,
				dirs = self.dirs,
				qty = self.qty
			}))
			return tail()
		end
	end

	-- For invalid or empty output, remove the metadata field.
	m:set_string(rotpower_key, nil)
	return tail()
end

------------------------------------------------------------------------
-- CONVERSION HELPERS

-- Convert to a string.  Also the default formatting for display.
function sz_rotpower:__tostring()
	local dstr = ""
	for k, v in pairs(sz_pos.dirs) do
		if self:dir_get(v) then
			dstr = dstr .. k
		end
	end
	return "rotpower " .. (self.qty or 0)
		.. " at " .. (self.pos and tostring(self.pos) or "(unk)")
		.. " to " .. dstr
end

------------------------------------------------------------------------
return sz_rotpower
