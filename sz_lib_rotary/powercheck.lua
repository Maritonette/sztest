-- LUALOCALS < ---------------------------------------------------------
local math, minetest, sz_pos
    = math, minetest, sz_pos
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

------------------------------------------------------------------------
-- RELATED EXTENSIONS

-- Extension to sz_pos: trigger a rotary power check at this location.
function sz_pos:rotary_power_check()
	self:node_signal("rotary_power_check", self:node_get())
end

------------------------------------------------------------------------
-- AUTOMATIC ACTIVE BLOCK MODIFIERS

-- Automatically do rotary power checks every so often for existing
-- blocks, in case they've ended up in an inconsistent state.  Select a
-- limited sample of fairly chosen nodes each sampling period, to ensure
-- an upper bound on the amount of CPU this checking can consume, as
-- rotary power checks may be very involved for some nodes, or cause
-- large networks of nodes to change power state.

local max = 100
local qty = 0
local ring = { }
local idx = { }
local hash = minetest.hash_node_position
minetest.register_abm({
	nodenames = { "group:sz_rotary" },
	interval = 10,
	chance = 1,
	action = function(pos, node)
		-- Queue new node registrations in the "ring buffer".  If
		-- the buffer is already full, then possibly replace an
		-- existing registration.
		qty = qty + 1
		if qty <= max then
			ring[qty] = pos
			idx[hash(pos)] = true
		elseif math_random(1, qty) <= max then
			local repkey = math_random(1, max)
			idx[hash(ring[repkey])] = nil
			ring[repkey] = pos
			idx[hash(pos)] = true
		end
	end
})

-- Each tick, try to process a small number of rotary power checks automatically.
-- If we run out of queued checks, pull some in from the ring buffer.
local curq = { }
local curp = 1
minetest.register_globalstep(function()
	if curp > #curq then
		curq = ring
		curp = 1
		ring = { }
		idx = { }
		qty = 0
	end
	for i = 0, 10 do
		local now = curq[curp]
		if now then
			sz_pos:new(now):rotary_power_check()
		end
		curp = curp + 1
	end
end)
