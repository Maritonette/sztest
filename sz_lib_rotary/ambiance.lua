-- LUALOCALS < ---------------------------------------------------------
local math, minetest, sz_pos
    = math, minetest, sz_pos
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

-- Play "gear" sounds periodically for all rotary machines that
-- contain gears.

local groupname = "sz_rotary_ambiance"

minetest.register_abm({
		nodenames = { "group:" .. groupname },
		interval = 1,
		chance = 10,
		catch_up = false,
		action = function(pos)
			minetest.after(math_random(), function()
				sz_pos:new(pos):sound("sz_lib_rotary_gears",
					{gain = math_random() * (pos:groups()[groupname] or 1) / 4})
			end)
		end
	})
