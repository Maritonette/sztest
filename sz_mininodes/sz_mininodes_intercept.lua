-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, pairs, stairs, sz_mininodes, sz_table, table,
      type
    = ipairs, minetest, pairs, stairs, sz_mininodes, sz_table, table,
      type
-- LUALOCALS > ---------------------------------------------------------

-- Handle the naming convention used by the upstream stairs mod.

local pref_mod = "stairs:"
local pref_stair = pref_mod .. "stair_"
local pref_slab = pref_mod .. "slab_"
local function isnotupsidedown(name)
	return not name:endswith("upside_down")
end
local function getsubname(name, pref)
	if not name then return end
	if name:startswith(":") then name = name:sub(2) end
	if not name:startswith(pref) then return end
	return name:sub(pref:len() + 1)
end

------------------------------------------------------------------------
-- Define custom logic to use when registering a node as stairs or as a slab.
-- Automatically apply texture and drop modification hooks from sz_mininodes,
-- and create appropriate mininodes.

-- Note that we call this after making slabs as well as after making stairs,
-- since it might not have worked the first call on the stairs, if the slab
-- was not yet defined, since mininodes need both.
local function makeminis(subname, ...)
	-- If stairs are defined for this subname, make mininodes.
	local stairdef = minetest.registered_nodes[pref_stair .. subname]
	if stairdef then
		local recipeitem = sz_mininodes.recipeitem_cache(subname)
		if recipeitem then
			sz_mininodes.register_mininodes(subname, stairdef, recipeitem)

			-- Also, make sure the recipeitem node itself can be
			-- cut into slabs by a saw, useful in the event you don't
			-- have 3 of them to complete the normal recipe.
			local rn = minetest.registered_nodes[recipeitem]
			if rn then
				rn = sz_table.copy(rn)
				rn.sawdrops = { [pref_slab .. subname] = 2 }
				minetest.register_node(":" .. recipeitem, rn)
			end
		end
	end

	return ...
end

local allowgroups = {
	precision_craft = 1,
	sz_slime_soluble = 1,
	sz_slime_permeable = 1,
	wet = 1,
	hot = 1,
	flammable = 1,
	igniter = 1,
	puts_out_fire = 1,
	not_in_creative_inventory = 1,
	oddly_breakable_by_hand = 1,
	choppy = 1,
	cracky = 1,
	snappy = 1,
	crumbly = 1,
	bendy = 1,
	melty = 1,
	fleshy = 1,
	level = 1,
	falling_node = 1
}
local function deftweaks(name, def)
	-- Remove any groups used in crafting recipes, to prevent cut nodes from
	-- being uses as substitutes for their full-size counterparts to produce
	-- full-sized crafting outputs.
	if name:startswith(":stairs:") or name:startswith("stairs:")
		or name:startswith("sz_mininodes:") then
		def.groups = sz_table.copy(def.groups)

		-- For now, this is a whitelist of inheritable material
		-- group names.
		local oldg = def.groups
		def.groups = {}
		for k, v in pairs(allowgroups) do
			def.groups[k] = oldg[k]
		end
	end
	
	return def
end
local function custom_reg_stair(oldreg, subname, name, def, ...)
	local dropas = sz_mininodes.custom_drop_get(subname)
	def.sawdrops = {
		["sz_mininodes:anulette_" .. subname] = 1,
		[pref_slab .. subname] = 1
	}
	def.drop = pref_stair .. dropas
	def.tiles = sz_mininodes.custom_overlay_get(def.tiles,
		{ "h", nil, "l", "r", nil, "b" })
	local meltsto = sz_mininodes.recipeitem_cache(subname)
	if meltsto then def.meltsto = { [meltsto] = 0.75 } end
	return makeminis(subname, oldreg(name, deftweaks(name, def), ...))
end
local function custom_reg_slab(oldreg, subname, name, def, ...)
	local dropas = sz_mininodes.custom_drop_get(subname)
	def.sawdrops = {
		["sz_mininodes:anulette_" .. subname] = 2
	}
	def.drop = pref_slab .. dropas
	def.tiles = sz_mininodes.custom_overlay_get(def.tiles,
		{ nil, nil, "b", "b", "b", "b" })
	local meltsto = sz_mininodes.recipeitem_cache(subname)
	if meltsto then def.meltsto = { [meltsto] = 0.5 } end
	return makeminis(subname, oldreg(name, deftweaks(name, def), ...))
end

------------------------------------------------------------------------
-- Intercept the registration of stair or slab nodes.  If either are being
-- registered, apply the appropriate custom logic.
local old_regnode = minetest.register_node
minetest.register_node = function(name, ...)
	if isnotupsidedown(name) then
		local stairsub = getsubname(name, pref_stair)
		if stairsub then
			return custom_reg_stair(old_regnode, stairsub, name, ...)
		end
		local slabsub = getsubname(name, pref_slab)
		if slabsub then
			return custom_reg_slab(old_regnode, slabsub, name, ...)
		end
	end
	return old_regnode(name, ...)
end

------------------------------------------------------------------------
-- Wrap the upstream stair/slab registration functions: add the ability
-- to automatically deduce parameters 3 and beyond, based on the material
-- being used in the recipe, assuming it's a node.  Keep a cache of recipe
-- item, so that we can re-register stairs safely, even after alternate
-- recipes have been defined.

local function paramwrap(oldfunc, parammod)
	return function(...) oldfunc(parammod(...)) end
end
local function make_paramwrap(subtype, desctype)
	return function(subname, recipeitem, groups, images, desc, sounds, ...)
		recipeitem = sz_mininodes.recipeitem_cache(subname, recipeitem)
		local reg = minetest.registered_nodes[pref_mod .. subtype .. "_" .. subname]
		local adddesc = ""
		if not reg then
			reg = minetest.registered_nodes[recipeitem]
			adddesc = " " .. desctype
		end
		if reg then
			groups = groups or reg.groups
			images = images or reg.tiles
			desc = desc or reg.description .. adddesc
			sounds = sounds or reg.sounds
		end

		-- XXX: Bug in upstream stairs?  If backface_culling is already
		-- set for a texture, then it's OMITTED from the definition
		-- instead of copied verbatim!
		for i, v in ipairs(images) do
			if type(v) == "table" then
				v = table.copy(v)
				v.backface_culling = nil
			end
			images[i] = v
		end

		return subname, recipeitem, groups, images, desc, sounds, ...
	end
end
stairs.register_stair = paramwrap(stairs.register_stair, make_paramwrap("stair", "Stair"))
stairs.register_slab = paramwrap(stairs.register_slab, make_paramwrap("slab", "Slab"))

------------------------------------------------------------------------
-- Re-register any existing stairs/slabs, to ensure that any custom wrapped
-- logic is correctly installed for upstream materials that were registered
-- before these wrappers were installed.

local function single(list)
	local unique
	for k, v in pairs(list) do
		if v ~= "" then
			if not unique then
				unique = v
			elseif v ~= unique then
				return
			end
		end
	end
	return unique
end
local function deduce_recipeitem(name)
	local recs = minetest.get_all_craft_recipes(name)
	if recs and #recs > 0 then
		for _, r in pairs(recs) do
			if r and r.items and r.type == "normal" then
				local recipeitem = single(r.items)
				if recipeitem then return recipeitem end
			end
		end
	end
end
local function rereg(pref, regfunc)
	for k, v in pairs(sz_table.copy(minetest.registered_nodes)) do
		if isnotupsidedown(k) then
			local subname = getsubname(k, pref)
			if subname then
				local recipeitem = sz_mininodes.recipeitem_cache(subname)
					or deduce_recipeitem(k)
				regfunc(subname, recipeitem, v)
			end
		end
	end
end
rereg(pref_stair, function(subname, recipeitem, v)
	stairs.register_stair(subname, recipeitem, v.groups, v.tiles, v.description, v.sounds)
end)
rereg(pref_slab, function(subname, recipeitem, v)
	stairs.register_slab(subname, recipeitem, v.groups, v.tiles, v.description, v.sounds)
end)
sz_mininodes.reregister_all()
