-- LUALOCALS < ---------------------------------------------------------
local minetest, stairs, sz_mininodes
    = minetest, stairs, sz_mininodes
-- LUALOCALS > ---------------------------------------------------------

-- Maintain a cache of registered stairs and recipe items; the
-- "intercept" submodule contains hooks to populate the cache from all
-- past and future registrations.
sz_mininodes.recipeitems = {}
function sz_mininodes.recipeitem_cache(name, val)
	if val then
		sz_mininodes.recipeitems[name] = val
		return val
	end
	return sz_mininodes.recipeitems[name]
end

-- Helper method to register all stairs, slabs, and mininodes,
-- with the appropriate custom drops if needed.
function sz_mininodes.register(mod, name, custdrop, tiles)
	if custdrop then sz_mininodes.custom_drop_set(name, custdrop) end
	local already = minetest.registered_nodes["stairs:slab_" .. name]
	if not already then
		stairs.register_slab(name, mod .. ":" .. name, nil, tiles)
	end
	already = minetest.registered_nodes["stairs:stair_" .. name]
	if not already then
		stairs.register_stair(name, mod .. ":" .. name, nil, tiles)
	end
end

-- Load other sub-modules.
sz_mininodes:loadlibs_prefix("sz_mininodes_",
	"custom",
	"define",
	"intercept"
)
