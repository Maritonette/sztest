-- LUALOCALS < ---------------------------------------------------------
local default, ipairs, minetest, pairs, sz_pos, sz_stuffpile, sz_table,
      type
    = default, ipairs, minetest, pairs, sz_pos, sz_stuffpile, sz_table,
      type
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local pile_formspec =
	"size[8,9]" ..
	default.gui_bg ..
	default.gui_bg_img ..
	default.gui_slots ..
	"list[current_name;main;0,0.3;8,4;]" ..
	"list[current_player;main;0,4.85;8,1;]" ..
	"list[current_player;main;0,6.08;8,3;8]" ..
	"listring[current_name;main]" ..
	"listring[current_player;main]" ..
	default.get_hotbar_bg(0, 4.85)

------------------------------------------------------------------------
-- UTILITY FUNCTIONS

-- Hash the contents of an inventory, to detect changes.
local function invhash(list)
	local t = sz_table:new()
	for i, v in ipairs(list) do
		t:insert(v:to_string())
	end
	return t:concat(";")
end

-- Standard checks to run periodically against a pile of stuff.
local function stuff_check(pos)
	pos = sz_pos:new(pos)

	-- If we're not standing on a solid walkable node, items
	-- break apart and fall down through.
	local below = pos:add(sz_pos.dirs.d):nodedef()
	if below and not below.walkable or below.liquidtype ~= "none" then
		pos:node_signal("selfdestruct")
		return
	end

	-- Can't have a pile of nothing.
	local meta = pos:meta()
	local inv = meta:get_inventory()
	if inv:is_empty("main") then
		pos:node_set()
		return
	end

	-- If any contents added/removed since our last check, then
	-- shuffle everything around.  It's a pile of junk, and no
	-- substitute for an organized container like a chest.
	if not meta:get_string("infotext") or
		meta:get_string("invhash") ~= invhash(inv:get_list("main")) then
		local t = sz_table:new(inv:get_list("main"))
		t:shuffle()
		inv:set_list("main", t)
		meta:set_string("invhash", invhash(t))

		-- Update the node's infotext with a summary of the items
		-- laying about.
		local n = sz_table:new()
		local len = 0
		local more = ""
		for i, v in ipairs(t) do
			local d = v and v:get_name()
			d = d and minetest.registered_items[d]
			d = d and d.description
			if d and #d > 0 and not n[d] then
				len = len + #d + 2
				if len > 100 then
					more = "..."
					break
				end
				n[d] = true
			end
		end
		meta:set_string("infotext", "Pile of: " ..
			n:keys():concat(", ") .. more)
	end
end

------------------------------------------------------------------------
-- NODE DEFINITION

-- A node representing a pile of stuff, implemented as an inventory node
-- instead of a bunch of loose objects.
minetest.register_node(sz_stuffpile.nodename, {
	description = "Pile of Stuff",
	drawtype = "signlike",
	tiles = { modname .. ".png" },
	inventory_image = modname .. ".png",
	drop = "",
	paramtype = "light",
	paramtype2 = "wallmounted",
	walkable = false,
	propagates_sunlight = true,
	selection_box = {
		type = "fixed",
		fixed = { -7/16, -0.5, -7/16, 7/16, -7/16, 7/16 },
	},
	on_construct = function(pos)
		local meta = minetest.get_meta(pos)
		meta:set_string("formspec", pile_formspec)
		meta:get_inventory():set_size("main", 32)
		minetest.after(0, function() stuff_check(pos) end)
	end,
	on_punch = function(pos, node, puncher)
		local pinv = puncher and puncher:get_inventory()
		if not pinv then
			return minetest.node_punch(pos, node, puncher)
		end

		local inv = sz_pos:new(pos):meta():get_inventory()
		local t = inv:get_list("main")
		for i, v in pairs(t) do
			t[i] = pinv:add_item("main", v)
		end
		inv:set_list("main", t)

		stuff_check(pos)
	end,
	selfdestruct = function(pos)
		pos = sz_pos:new(pos)
		pos:inv_eject("main")
		pos:node_set()
	end,
	on_metadata_inventory_take = stuff_check,
	on_metadata_inventory_put = stuff_check,
	on_metadata_inventory_move = stuff_check,
	sounds = default.node_sound_dirt_defaults(),
	groups = {
		items_on_ground = 1,
		not_in_creative_inventory = 1
	}
})

-- Periodically re-check the stuff piles for validity, in case a change
-- slipped through our checks.
minetest.register_abm({
	nodenames = { sz_stuffpile.nodename },
	interval = 1,
	chance = 1,
	action = stuff_check
})
