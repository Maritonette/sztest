-- LUALOCALS < ---------------------------------------------------------
-- SKIP: sz_stuffpile
local sz_class
    = sz_class
-- LUALOCALS > ---------------------------------------------------------

sz_class:loadsubclasses(
	"sz_stuffpile"
)
sz_class:loadlibs(
	"pilenode"
)
