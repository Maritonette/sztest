-- LUALOCALS < ---------------------------------------------------------
local default, minetest, pairs, sz_facedir, sz_pos, sz_table, type
    = default, minetest, pairs, sz_facedir, sz_pos, sz_table, type
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- COSMOLOGICAL SENSOR BLOCK

local sensor_0_name = modname .. ":sensor"
local sensor_1_name = modname .. ":sensor_1"
local sensor_2_name = modname .. ":sensor_2"
local sensor_3_name = modname .. ":sensor_3"

-- Construct an index to quickly look up the appropriate
-- node type to use for sensors with mapblock boundaries
-- in the given directions.

local sensors_idx = { }
sensors_idx[0] = { name = sensor_0_name, param2 = 0 }

local function dirhash(u, d, n, s, e, w)
	local i = 0
	if u then i = i + 1 end
	if d then i = i + 2 end
	if n then i = i + 4 end
	if s then i = i + 8 end
	if e then i = i + 16 end
	if w then i = i + 32 end
	return i
end

for f = 0, 23 do
	local fd = sz_facedir:from_param(f)

	local t = { }
	for k, v in pairs(sz_pos.dirs) do
		if fd:front():eq(v) then
			t[k] = true
		end
	end
	sensors_idx[dirhash(t.u, t.d, t.n, t.s, t.e, t.w)]
		= { name = sensor_1_name, param2 = f }

	local t = { }
	for k, v in pairs(sz_pos.dirs) do
		if fd:front():eq(v)
			or fd:top():eq(v) then
			t[k] = true
		end
	end
	sensors_idx[dirhash(t.u, t.d, t.n, t.s, t.e, t.w)]
		= { name = sensor_2_name, param2 = f }

	local t = { }
	for k, v in pairs(sz_pos.dirs) do
		if fd:front():eq(v) or fd:top():eq(v)
			or fd:right():eq(v) then
			t[k] = true
		end
	end
	sensors_idx[dirhash(t.u, t.d, t.n, t.s, t.e, t.w)]
		= { name = sensor_3_name, param2 = f }
end

-- After placing a sensor, immediately check its position for
-- mapblock boundaries, and replace the node with the correct
-- sensor, unless already correct.

local function sensor_check(pos)
	pos = sz_pos:new(pos)

	if not pos:groups().sz_cosmo_sensor then return end

	local oldnode = pos:node_get()

	local px = pos.x % 16
	local py = pos.y % 16
	local pz = pos.z % 16
	local hash = dirhash(
		py == 15,
		py == 0,
		pz == 15,
		pz == 0,
		px == 15,
		px == 0)
	local sens = sensors_idx[hash]

	if sens.name == oldnode.name
		and sens.param2 == oldnode.param2 then
		return
	end
	
	pos:node_set(sens)
end

-- Register the various sensor nodes.

local function reg_sensor(name, def)
	return minetest.register_node(name, sz_table.mergedeep(def or {}, {
		description = "Cosmosthesium",
		drop = sensor_0_name,
		groups = { cracky = 1, oddly_breakable_by_hand = 1, sz_cosmo_sensor = 1 },
		sounds = default.node_sound_stone_defaults(),
		paramtype = "light",
		paramtype2 = "facedir",
		on_construct = sensor_check
	}))
end

local function anim(name, length, size, type)
	length = length or 2
	size = size or 16
	type = type or "vertical_frames"
	return {
		name = name,
		animation = {
			type = type,
			aspect_w = size,
			aspect_h = size,
			length = length
		}
	}
end

reg_sensor(sensor_0_name, {tiles = {"sz_cosmo_sensor_0.png"}})
reg_sensor(sensor_1_name, {
	tiles = {
		anim("sz_cosmo_sensor_1v.png^[transformR180"),
		anim("sz_cosmo_sensor_1v.png"),
		anim("sz_cosmo_sensor_1h.png^[transformR180"),
		anim("sz_cosmo_sensor_1h.png"),
		"sz_cosmo_sensor_0.png",
		anim("sz_cosmo_sensor_3.png")
	},
	light_source = 2,
	groups = {not_in_creative_inventory = 1}
})
reg_sensor(sensor_2_name, {
	tiles = {
		anim("sz_cosmo_sensor_3.png"),
		anim("sz_cosmo_sensor_1v.png"),
		anim("sz_cosmo_sensor_2.png"),
		anim("sz_cosmo_sensor_2.png^[transformFX"),
		anim("sz_cosmo_sensor_1v.png"),
		anim("sz_cosmo_sensor_3.png")
	},
	light_source = 4,
	groups = {not_in_creative_inventory = 1}
})
reg_sensor(sensor_3_name, {
	tiles = {
		anim("sz_cosmo_sensor_3.png"),
		anim("sz_cosmo_sensor_2.png^[transformFX"),
		anim("sz_cosmo_sensor_3.png"),
		anim("sz_cosmo_sensor_2.png^[transformFX"),
		anim("sz_cosmo_sensor_2.png"),
		anim("sz_cosmo_sensor_3.png")
	},
	light_source = 6,
	groups = {not_in_creative_inventory = 1}
})

------------------------------------------------------------------------
-- NODE RECIPES

minetest.register_craft({
	output = modname .. ":sensor",
	recipe = {
		{ "", "dye:violet", "" },
		{ "dye:violet", "default:obsidian", "dye:violet" },
		{ "", "dye:violet", "" },
	}
})
