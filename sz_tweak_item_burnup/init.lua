-- LUALOCALS < ---------------------------------------------------------
local minetest, sz_pos
    = minetest, sz_pos
-- LUALOCALS > ---------------------------------------------------------

local bitemname = "__builtin:item"
local bitem = minetest.registered_entities[bitemname]

local old_step = bitem.on_step
bitem.on_step = function(self, dtime, ...)
	old_step(self, dtime, ...)

	local pos = sz_pos:new(self.object:getpos())
	local posdef = pos:nodedef()

	-- Items are now flammable, and will not survive lava or fire.
	if posdef and posdef.groups and posdef.groups.igniter then
		self.object:remove()
		local vel = sz_pos:xyz(1, 1, 1)
		pos:smoke(5, vel, { maxsize = 4 })
		pos:sound("default_cool_lava", { gain = 0.25 })
		return
	end
end

minetest.register_entity(":"..bitemname, bitem)
