-- LUALOCALS < ---------------------------------------------------------
local default, ipairs, math, minetest, pairs, sz_pos
    = default, ipairs, math, minetest, pairs, sz_pos
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local flame_name = modname .. ":hot_flame_1"
local venturi_name = modname .. ":venturi"

-- Check the flame status of the venturi, igniting it
-- or extinguishing it as appropriate.
local function venturi_check(pos, node)
	pos = sz_pos:new(pos)
	local below = pos:add(sz_pos.dirs.d)
	local above = pos:add(sz_pos.dirs.u)

	-- If the gas is turned off, or the flame is extinguished
	-- externally e.g. by water:
	if not below:groups().sz_coalgas
		or not above:fire_allowed() then

		-- Remove any existing flame.
		if above:groups()[modname .. "_flame"] then
			above:node_set()
		end

		-- Don't process ignition.
		return
	end

	-- If there is no flame above, push gas out the top.
	if not above:groups()[modname .. "_flame"] then
		local didflame

		-- If there is an ignition source, push out fire.
		if above:fire_allowed() then
			for k, v in pairs(above:nodes_in_area(sz_pos:xyz(2, 2, 2),
				{ "group:igniter", "group:weak_igniter" })) do
				above:node_set({ name = flame_name })
				didflame = true
				break
			end
		end

		-- If there is no ignition source, but there's just plain air, then
		-- randomly dump some gas up above, to make these things dangerous
		-- to work with if the pilot goes out but the gas is not shut off.
		if not didflame and above:is_empty() and math_random(1, 50) == 1 then
			above:scan_flood(1, function(p)
				if not p:is_empty() then return false end
				p:node_set({ name = modname .. ":gas_flowing", param2 = 7 })
			end)
		end
	end

	-- Return true to signal to caller that the flame
	-- is enabled.
	return true
end

-- Register the venturi node itself.
minetest.register_node(venturi_name, {
	description = "Venturi",
	tiles = {
		modname .. "_top.png",
		modname .. "_bottom.png",
		"default_steel_block.png"
	},
	groups = {
		[modname .. "_flame_source_hot_flame_1"] = 1,
		cracky = 1,
	},
	on_construct = venturi_check,
	[modname .. "_extinguished"] = venturi_check,
	after_destruct = function(pos)
		local above = sz_pos:new(pos):add(sz_pos.dirs.u)
		above:node_signal(modname .. "_flame_check",
			above:node_get())
	end,
	sounds = default.node_sound_stone_defaults()
})

-- An abm to periodically do the flame check, plus a random
-- chance of a flashback if there is flame above.
minetest.register_abm({
	nodenames = { venturi_name },
	interval = 1,
	chance = 2,
	action = function(pos)
		if venturi_check(pos) then
			-- There's a small random chance of flashback; installing
			-- some kind of arrestor would be highly advisable.
			local below = sz_pos:new(pos):add(sz_pos.dirs.d)
			if math_random(1, 100) == 1 and below:fire_allowed() then
				below:node_set({ name = "fire:basic_flame" })
				below:add(sz_pos.dirs.u:scale(2)):node_set()
			end
		end
	end
})

-- Recipes.
for i, v in ipairs({"", "_flat"}) do
	minetest.register_craft({
		output = modname .. ":venturi",
		recipe = {
			{ "default:steel_ingot", "xpanes:bar"..v, "default:steel_ingot" },
			{ "default:steel_ingot", "", "default:steel_ingot" },
			{ "default:steel_ingot", "xpanes:bar"..v, "default:steel_ingot" },
		}
	})
end
