-- LUALOCALS < ---------------------------------------------------------
local io, minetest, pairs
    = io, minetest, pairs
local io_close, io_open
    = io.close, io.open
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local datapath = minetest.get_worldpath() .. "/" .. modname .. ".txt"
local enabled = {}
do
	local f = io_open(datapath, "rb")
	if f then
		enabled = minetest.deserialize(f:read("*all"))
		io_close(f)
	end
end
local function savedata()
	local f = io_open(datapath, "wb")
	f:write(minetest.serialize(enabled))
	io_close(f)
end

minetest.register_privilege("autotool", "Can enable automatic tool selection")

minetest.register_chatcommand("autotool", {
	privs = {autotool = true},
	description = "Enable/disable auto tool selection",
	func = function(name, param)
		if param == "on" and not enabled[name] then
			enabled[name] = true
			savedata()
		end
		if param == "off" and enabled[name] then
			enabled[name] = nil
			savedata()
		end
		if enabled[name] then
			return minetest.chat_send_player(name,
				"Automatic tool selection is enabled; \"/autotool off\" to disable")
		end
		minetest.chat_send_player(name,
			"Automatic tool selection is disabled; \"/autotool on\" to enable")
	end})

local restore = {}

minetest.register_globalstep(function(dtime)
		local newrest = {}
		for k, v in pairs(minetest.get_connected_players()) do
			local n = v:get_player_name()
			local r = restore[n]
			if r then
				if v:get_player_control().LMB then
					newrest[n] = r
				else
					r(v)
				end
			end
		end
		restore = newrest
	end)

local function toolcap(t, ng)
	local c = t:get_tool_capabilities()
	if not c then return end
	local l = ng.level or 0
	for k, v in pairs(c.groupcaps) do
		local gn = ng[k]
		if v.maxlevel >= l and gn and v.times[gn] then
			return {
				caps = v,
				lv = v.maxlevel,
				group = k,
				gnum = gn,
				time = v.times[gn],
				wear = t:get_wear()
			}
		end
	end
end

local function invswap(inv, n1, i1, n2, i2)
	local s1 = inv:get_stack(n1, i1)
	local s2 = inv:get_stack(n2, i2)
	inv:set_stack(n1, i1, s2)
	inv:set_stack(n2, i2, s1)
end

local function better(a, b)
	if not b then return end
	if not a then return true end

	-- Cheapest tool...
	if a.lv < b.lv then return end
	if b.lv < a.lv then return true end

	-- ...then fastest tool...
	if a.time < b.time then return end
	if a.time > b.time then return true end

	-- ...then least worn (wear-leveling)
	if a.wear < b.wear then return end
	if a.wear > b.wear then return true end
end

minetest.register_on_punchnode(function(pos, node, puncher, pointed)
		if not puncher or not puncher:is_player() then return end
		local pname = puncher:get_player_name()
		if not minetest.check_player_privs(pname, {autotool = true})
			or not enabled[pname] then return end

		local rf = restore[pname]
		if rf then
			rf(puncher)
			restore[pname] = nil
		end

		local n = minetest.get_node_or_nil(pos)
		if not n then return end
		local r = minetest.registered_nodes[n.name]
		if not r then return end
		local ng = r.groups or {}

		local best
		local inv = puncher:get_inventory()
		for i = 1, inv:get_size("main") do
			local t = toolcap(inv:get_stack("main", i), ng)
			if better(best, t) then
				best = t
				best.i = i
			end
		end
		if best then
			local wl = puncher:get_wield_list()
			local wi = puncher:get_wield_index()
			if wl ~= "main" or wi ~= best.i then
				restore[pname] = function(p)
					return invswap(p:get_inventory(), wl, wi, "main", best.i)
				end
				return invswap(inv, wl, wi, "main", best.i)
			end
		end
	end)
