-- LUALOCALS < ---------------------------------------------------------
local ItemStack, ipairs, minetest, pairs, sz_table, type
    = ItemStack, ipairs, minetest, pairs, sz_table, type
-- LUALOCALS > ---------------------------------------------------------

-- Sort helper function to sort items by values looked up in a
-- separate table, e.g. for sorting lists of keys by their values.
local function sortby(tbl)
	return function(a, b)
		if tbl[a] == tbl[b] then return a < b end
		return tbl[a] > tbl[b]
	end
end

-- Helper function to create a report from the sorted keys and values
-- of a table, given a custom sort function.
local function report(tbl, sortfunc)
	tbl = sz_table:new(tbl)
	local keys = tbl:keys()
	keys:sort(sortfunc)
	local rpt = sz_table:new()
	for i, v in ipairs(keys) do
		rpt:insert(tbl[v] .. " " .. v)
	end
	return rpt:concat(", ")
end

------------------------------------------------------------------------
-- REGISTER CHAT COMMANDS

minetest.register_chatcommand("regstats", {
	privs = {server = true},
	description = "Statistics about total registered things.",
	func = function(name)
		local regpref = "registered_"
		local regged = sz_table:new()
		for k, v in pairs(minetest) do
			if k:startswith(regpref) and type(v) == "table" then
				local q = 0
				for ik, iv in pairs(v) do
					q = q + 1
				end
				regged[k:sub(regpref:len() + 1)] = q
			end
		end
		minetest.chat_send_player(name, "registration count: "
			.. report(regged))
	end
})

minetest.register_chatcommand("regnodes", {
	privs = {server = true},
	description = "Statistics about total registered nodes, by mod.",
	func = function(name)
		local rn = sz_table:new({ TOTAL = 0 })
		for k, v in pairs(minetest.registered_nodes) do
			local mod = k
			local idx = k:find(":", 1, true)
			mod = idx and mod:sub(1, idx - 1) or "BUILTIN"
			rn[mod] = (rn[mod] or 0) + 1
			rn.TOTAL = rn.TOTAL + 1
		end
		minetest.chat_send_player(name, "registered nodes by mod: "
			.. report(rn, sortby(rn)))
	end
})

minetest.register_chatcommand("reggroups", {
	privs = {server = true},
	description = "Statistics about total registered nodes, by group.",
	func = function(name)
		local rn = sz_table:new({ })
		for k, v in pairs(minetest.registered_nodes) do
			if v.groups then
				for g, n in pairs(v.groups) do
					rn[g] = (rn[g] or 0) + 1
				end
			end
		end
		minetest.chat_send_player(name, "registered nodes by group: "
			.. report(rn, sortby(rn)))
	end
})

minetest.register_chatcommand("regcrafts", {
	privs = {server = true},
	description = "Statistics about total registered recipes, by type and mod.",
	func = function(name)
		local rn = sz_table:new({ })
		for k, v in pairs(minetest.registered_items) do
			local mod = k
			local idx = k:find(":", 1, true)
			mod = idx and mod:sub(1, idx - 1) or "BUILTIN"
			for i, r in ipairs(minetest.get_all_craft_recipes(k) or {}) do
				local t = rn[r.method or r.type] or {}
				rn[r.type] = t
				t[mod] = (t[mod] or 0) + 1
			end
			local f = minetest.get_craft_result({method = "fuel",
				width = 1, items = {ItemStack(k)}})
			if f and f.time and f.time > 0 then
				local t = rn.fuel or {}
				rn.fuel = t
				rn.fuel[mod] = (rn.fuel[mod] or 0) + 1
			end
		end
		local types = rn:keys()
		types:sort()
		for i, v in ipairs(types) do
			minetest.chat_send_player(name, "registered " .. v .. " recipes by mod: "
				.. report(rn[v], sortby(rn[v])))
		end
	end
})
