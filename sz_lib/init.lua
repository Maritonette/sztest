-- LUALOCALS < ---------------------------------------------------------
-- SKIP: sz_class sz_table
local dofile, ipairs, math, minetest, os, print, rawget, rawset,
      setmetatable
    = dofile, ipairs, math, minetest, os, print, rawget, rawset,
      setmetatable
local math_floor, os_clock
    = math.floor, os.clock
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- LOAD TIMING DIAGNOSTIC

local startuptime = os_clock()
print(modname .. ": load time begin")
minetest.after(0, function()
	local ms = math_floor((os_clock() - startuptime) * 1000000 + 0.5) / 1000
	print(modname .. ": load time ended in " .. ms .. "ms")
end)

------------------------------------------------------------------------
-- SZ_CLASS BASE CLASS DEFINITION

local sz_class = { }
rawset(_G, "sz_class", sz_class)
sz_class.__index = sz_class

function sz_class:new(init)
	init = init or { }
	setmetatable(init, self)
	return init
end

function sz_class:subclass(...)
	for i, name in ipairs({...}) do
		if not name then return end
		local sub = self:new(rawget(_G, name))
		sub.__index = sub
		rawset(_G, name, sub)
	end
end

sz_class.loadedlibs = {}
function sz_class:loadlibs(...)
	local modname = minetest.get_current_modname()
	local modpath = minetest.get_modpath(modname) .. "/"
	for i, class in ipairs({...}) do
		local lib = modpath .. class .. ".lua"
		self.loadedlibs[lib] = self.loadedlibs[lib]
			or dofile(lib) or true
	end
end

function sz_class:loadlibs_prefix(prefix, ...)
	for i, class in ipairs({...}) do
		self:loadlibs(prefix .. class)
	end
end

function sz_class:loadsubclasses(...)
	self:subclass(...)
	return self:loadlibs(...)
end

------------------------------------------------------------------------
-- LOAD SUBCLASSES

sz_class:loadsubclasses("sz_table")
sz_table:loadsubclasses(
	"sz_util",
	"sz_file",
	"sz_pos",
	"sz_facedir",
	"sz_nodetrans"
)

sz_class:loadlibs(
	"ext_item_entity_sz_data",
	"ext_string"
)
