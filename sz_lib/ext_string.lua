-- LUALOCALS < ---------------------------------------------------------
local string
    = string
local string_find, string_len, string_lower, string_sub
    = string.find, string.len, string.lower, string.sub
-- LUALOCALS > ---------------------------------------------------------

-- Quick helper to tell if a string starts with a prefix
-- string, without all the sub/len mess-around-ery.
function string:startswith(pref)
	return string_sub(self, 1, string_len(pref)) == pref
end

-- Quick helper to tell if a string ends with a suffix
-- string, without all the sub/len mess-around-ery.
function string:endswith(suff)
	return suff == "" or string_sub(self, -string_len(suff)) == suff
end

-- Quick helper to tell if a string contains another string
-- anywhere inside.
function string:contains(substr, ignorecase)
	if ignorecase then
		return string_find(string_lower(self), string_lower(substr), 1, true)
	end
	return string_find(self, substr, 1, true)
end
