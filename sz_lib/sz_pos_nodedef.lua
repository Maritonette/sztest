-- LUALOCALS < ---------------------------------------------------------
local minetest, sz_pos, sz_table
    = minetest, sz_pos, sz_table
-- LUALOCALS > ---------------------------------------------------------

-- Get the definition of the node at this position, or nil if
-- there is no node here, or the node is not defined.
function sz_pos:nodedef()
	local n = self:node_get()
	if n == nil or n.name == nil then return end
	return minetest.registered_nodes[n.name]
end

-- If the definition of the node at this location has a registered hook
-- with the given name, trigger it with the given arguments.
local function signal(pos, hook, ...)
	local def = pos:nodedef()
	if not def then return end
	hook = def[hook]
	if hook then return hook(...) end
end
-- Newer "signal" semantics, always send node position argument.
function sz_pos:node_signal(hook, ...)
	return signal(self, hook, self, ...)
end
-- Legacy semantic, just send caller's params.
function sz_pos:nodedef_trigger(hook, ...)
	return signal(self, hook, ...)
end
	
-- A safe accessor to get the groups for the node definition
-- at this location that will always return a table.
function sz_pos:groups()
	return sz_table:new((self:nodedef() or { }).groups or { })
end

-- Return true if this location contains only air.
function sz_pos:is_empty()
	local node = self:node_get()
	return node and node.name == "air"
end
