-- LUALOCALS < ---------------------------------------------------------
local ItemStack, pairs, sz_util
    = ItemStack, pairs, sz_util
-- LUALOCALS > ---------------------------------------------------------

-- NodeMetaRef:to_table() apparently returns a "mixed" lua table with
-- some userdata refs mixed in with pure lua structures.  These methods
-- attempt to convert metadata to/from pure lua data, which can be
-- serialized and copied around freely.

-- Convert metadata to a pure lua table.
function sz_util.meta_to_lua(meta)
	if not meta then return end

	local t = meta:to_table()
	local o = {}

	-- Copy fields, if there are any.
	if t.fields then 
		for k, v in pairs(t.fields) do
			o.f = t.fields
			break
		end
	end

	-- Copy inventory, if there are any.
	local i = meta:get_inventory()
	for k, v in pairs(i:get_lists()) do
		o.i = o.i or {}

		local j = {}
		o.i[k] = j

		local s = i:get_size(k)
		j.s = s

		j.i = {}
		for n = 0, s do
			local x = i:get_stack(k, n)
			if x then j.i[n] = x:to_table() end
		end
	end

	-- Try to return nil, if possible, for an empty
	-- metadata table, otherwise return the data.
	for k, v in pairs(o) do return o end
end

-- Write a pure lua metadata table back into a NodeMetaRef.
function sz_util.lua_to_meta(lua, meta)
	-- Always clear the meta, and load the fields if any.
	local t = {fields = {}, inventory = {}}
	if lua and lua.f then t.fields = lua.f end
	meta:from_table(t)

	-- Load inventory, if any.
	if lua and lua.i then
		local i = m:get_inventory()
		for k, v in pairs(lua.i) do
			i:set_size(k, v.s)
			for sk, sv in pairs(v.i) do
				i:set_stack(ik, sk, ItemStack(sv))
			end
		end
	end
end
