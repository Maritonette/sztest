-- LUALOCALS < ---------------------------------------------------------
local math, minetest, pairs, sz_facedir, sz_pos
    = math, minetest, pairs, sz_facedir, sz_pos
local math_floor
    = math.floor
-- LUALOCALS > ---------------------------------------------------------

-- Convert to a string.  Also the default formatting for display.
sz_pos.to_string = minetest.pos_to_string
sz_pos.__tostring = minetest.pos_to_string

------------------------------------------------------------------------
-- FACEDIR

-- Lookup the "simple" facedir (not factoring in rotation) for this pos.
function sz_pos:back_to_facedir(...)
	return sz_facedir:from_vectors(self, ...)
end
function sz_pos:top_to_facedir(back, ...)
	return sz_facedir:from_vectors(back, self, ...)
end

------------------------------------------------------------------------
-- WALLMOUNTED

-- Convert to a "wallmounted" direction, which is like a facedir but
-- without rotation.
function sz_pos:to_wallmounted()
	return minetest.dir_to_wallmounted(self)
end

-- Create a new sz_pos from a wallmounted param2 value.
local wm_lookup = { }
function sz_pos:from_wallmounted(w)
	return wm_lookup[w]
end
for k, v in pairs(sz_pos.dirs) do
	wm_lookup[minetest.dir_to_wallmounted(v)] = v
end

------------------------------------------------------------------------
-- POS HASH

-- Compute a hash value, for hashtable lookup use.
sz_pos.hash = minetest.hash_node_position

-- Convert from node "hash" back to position.
function sz_pos:from_hash(h)
	return self:new(minetest.get_position_from_hash(h))
end

------------------------------------------------------------------------
-- MAPBLOCK

function sz_pos:mapblock()
	return sz_pos:new({
		x = math_floor((self.x + 0.5) / 16),
		y = math_floor((self.y + 0.5) / 16),
		z = math_floor((self.z + 0.5) / 16)
	})
end

function sz_pos:blockmin()
	return self:scale(16)
end

function sz_pos:blockmax()
	return sz_pos:new({
		x = self.x * 16 + 15,
		y = self.y * 16 + 15,
		z = self.z * 16 + 15
	})
end

function sz_pos:blockid()
	return self.x + 4096 * self.y + 16777216 * self.z
end

local function pymod(n, m)
	if n >= 0 then return n % m end
	return m - ((-n) % m)
end
function sz_pos:from_blockid(i)
	local x = pymod(i, 4096)
	i = (i - x) / 4096
	local y = pymod(i, 4096)
	i = (i - y) / 4096
	return sz_pos:xyz(x, y, i)
end
