-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, pairs, sz_pos, sz_table
    = ipairs, minetest, pairs, sz_pos, sz_table
-- LUALOCALS > ---------------------------------------------------------

-- Eject items from this location, optionally flying in random
-- directions.
function sz_pos:item_eject(stack, speed, qty)
	for i = 1, (qty or 1) do
		local obj = minetest.add_item(self:scatter(), stack)
		if obj then
			obj:setvelocity(sz_pos.zero:scatter():scale(speed or 0))
		end
	end
end

-- Eject all items in an inventory list from this location.
function sz_pos:inv_eject(listname, speed)
	local inv = self:inv()
	local list = inv:get_list(listname)
	if not list then return end
	for i, v in ipairs(list) do
		self:item_eject(v, speed)
		inv:set_stack(listname, i, "")
	end
end

-- An alternative to objects_in_radius that automatically excludes
-- players who don't have the "interact" privilege, i.e. are effectively
-- just spectators, and should not be "detected" by some code.
function sz_pos:tangible_in_radius(...)
	local t = sz_table:new()
	for k, v in pairs(self:objects_in_radius(...)) do
		if not v:is_player() or minetest.get_player_privs(
			v:get_player_name()).interact then
			t[k] = v
		end
	end
	return t
end

-- Hurt all entities within a radius of this location, with linear
-- fall-off, and an optional elliptoid shape.
function sz_pos:hitradius(r, hp, shape)
	-- Default shape if not specified to a sphere of the
	-- same radius as our search area.
	if shape then
		shape = sz_pos:new(shape):abs()
	else
		shape = sz_pos:xyz(r, r, r)
	end

	-- Degenerate elliptoid, no volume.  Skip the rest, since
	-- there's no actual damage volume, and we'd divide by 0.
	if shape.x == 0 or shape.y == 0 or shape.z == 0 then return end

	-- Scan for nearby objects.
	for k, v in pairs(self:tangible_in_radius(r)) do
		local p = self:sub(v:getpos())
		local d = sz_pos:xyz(
			p.x / shape.x,
			p.y / shape.y,
			p.z / shape.z):len()
		if d < 1 then
			v:set_hp(v:get_hp() - hp * (1 - d))
		end
	end
end

-- Shortcuts for some minetest utility functions.
sz_pos.objects_in_radius = minetest.get_objects_inside_radius
sz_pos.entity_add = minetest.add_entity
