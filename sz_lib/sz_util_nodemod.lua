-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, pairs, sz_table, sz_util, type
    = ipairs, minetest, pairs, sz_table, sz_util, type
-- LUALOCALS > ---------------------------------------------------------

-- Merge modifications into a node definition.  This works for current
-- and future registrations, by way of intercepting the
-- minetest.register_node method.
local nodemods = {}
function sz_util.modify_node(name, mod)
	-- Mods can be a table, to be merged over the original;
	-- convert to a function.
	if type(mod) == "table" then
		local modtbl = mod
		mod = function(old) return sz_table.mergedeep(modtbl, old) end
	end

	-- Mod functions should only apply to each node type once.
	local oldmod = mod
	local modsdone = {}
	mod = function(def, name, ...)
		if modsdone[name] then return def end
		modsdone[name] = true
		return oldmod(def, name, ...)
	end

	-- Add the mod to the mods table, for future matching
	-- registrations.
	local mods = nodemods[name]
	if not mods then
		mods = sz_table:new()
		nodemods[name] = mods
	end
	mods:insert(mod)

	-- Apply the mod to any existing registrations.
	local function modold(name, old)
		local mn = minetest.get_current_modname()
		if name:sub(1, mn:len() + 1) ~= (mn .. ":")
			and name:sub(1, 1) ~= ":" then
			name = ":" .. name
		end
		minetest.register_node(name, mod(old, name))
	end
	if name == "*" then
		for k, v in pairs(minetest.registered_nodes) do
			if k ~= "ignore" then modold(k, v) end
		end
	elseif minetest.registered_nodes[name] then
		modold(name, minetest.registered_nodes[name])
	end
end
local oldreg = minetest.register_node
minetest.register_node = function(name, def, ...)
	local function applymods(mods)
		if not mods then return end
		for i, v in ipairs(mods) do
			def = v(def, name)
		end
	end
	applymods(nodemods[name])
	applymods(nodemods["*"])
	return oldreg(name, def, ...)
end
