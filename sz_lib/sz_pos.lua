-- LUALOCALS < ---------------------------------------------------------
local sz_pos, sz_table
    = sz_pos, sz_table
-- LUALOCALS > ---------------------------------------------------------

-- This is a general-purpose 3d vector helper library, which represents
-- tuples of (x, y, z) coordinates, in both relative and absolute
-- contexts.

------------------------------------------------------------------------
-- CONSTRUCTORS AND STATIC PROPERTIES

-- Create a new sz_pos from loose coordinates declared in order.
function sz_pos:xyz(x, y, z)
	return self:new({ x = x, y = y or x, z = z or x })
end

-- Trivial zero vector.
sz_pos.zero = sz_pos:xyz(0, 0, 0)

-- All 6 cardinal directions in 3 dimensions.
sz_pos.dirs = sz_table:new({
	u = sz_pos:xyz(0, 1, 0),
	d = sz_pos:xyz(0, -1, 0),
	n = sz_pos:xyz(0, 0, 1),
	s = sz_pos:xyz(0, 0, -1),
	e = sz_pos:xyz(1, 0, 0),
	w = sz_pos:xyz(-1, 0, 0),
})

-- Get an array of all directions in random order.  Useful for things
-- that operate in a random direction, or more than one direction in
-- random order.
function sz_pos.shuffledirs()
	return sz_pos.dirs:values():shuffle()
end

sz_pos:loadlibs_prefix("sz_pos_",
	"arith",
	"convert",
	"environ",
	"limitfx",
	"node",
	"nodedef",
	"nodeshatter",
	"objects",
	"volume"
)
