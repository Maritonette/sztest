-- LUALOCALS < ---------------------------------------------------------
local ipairs, math, minetest, next, pairs, sz_pos, sz_table
    = ipairs, math, minetest, next, pairs, sz_pos, sz_table
local math_random
    = math.random
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- MOLTEN ITEM SLAG

-- Slag is a fluid that looks like lava, but is actually items in molten
-- form.  Quenching it will retrieve the contained items.

local srcname = modname .. ":slag_source"
local flowname = modname .. ":slag_flowing"

local function slagreg(name, proto, def)
	minetest.register_node(name, sz_table.mergedeep(def,
		sz_table.merge({
			description = "Slag",
			groups = {
				igniter = 1,
				[modname .. "_slag"] = 1
			},
			drop = "",
			liquid_alternative_flowing = flowname,
			liquid_alternative_source = srcname
		}, proto)
	))
end
local lavasrc = minetest.registered_nodes["default:lava_source"]
local lavaflow = minetest.registered_nodes["default:lava_flowing"]
slagreg(srcname, lavasrc, {
	buildable_to = false,
	on_construct = function(pos)
		minetest.get_meta(pos):get_inventory():set_size("main", 32)
	end
})
slagreg(flowname, lavaflow, { })

-- Water quenches slag, and retrieves the molten items stored
-- within.
minetest.register_abm({
	nodenames = { "group:" .. modname .. "_slag" },
	neighbors = { "group:water" },
	interval = 1,
	chance = 1,
	action = function(pos)
		pos = sz_pos:new(pos)

		-- Make sure we haven't already attempted to process
		-- this check recently, since it's expensive.
		local now = minetest.get_gametime()
		local next = pos:meta():get_float("nextcheck")
		if not next and next > now then return end
		next = next + math_random() * 5 + 5
		pos:meta():set_float("nextcheck", next)

		-- First, find the quenching water.
		local wpos = nil
		for k, v in pairs(sz_pos.dirs) do
			local p = pos:add(v)
			if p:groups().water then
				wpos = p
				break
			end
		end
		if not wpos then return end

		-- Now scan through the slag and try to find the
		-- original source.  Of course, there's a limit
		-- to how far we'll search; slag needs to be cooled
		-- reasonably close to the source.
		local spos = pos:scan_flood(16, function(p)
			local n = p:node_get().name
			if n == srcname then
				return p
			elseif n ~= flowname then
				return false
			end
		end)
		if not spos then return end

		local list = spos:meta():get_inventory():get_list("main")
		if list then
			for i, v in ipairs(list) do
				wpos:item_eject(v)
			end
		end
		local vel = sz_pos:xyz(1, 1, 1)
		pos:smoke(10, vel, { maxsize = 4 })
		pos:sound("default_cool_lava")
		spos:node_set({ name = flowname, param2 = 7 })
	end
})
