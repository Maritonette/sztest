-- LUALOCALS < ---------------------------------------------------------
local minetest, pairs, sz_pos, sz_stuffpile
    = minetest, pairs, sz_pos, sz_stuffpile
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- UTILITY FUNCTIONS

-- Drop all items, attempting to immediately settle them into piles (to
-- avoid item_entity_ttl jeopardy).
local function dropall(player)
	local pos = sz_pos:new(player:getpos())
	local inv = player:get_inventory()
	for _, n in pairs({ "main", "craft" }) do
		for i = 1, inv:get_size(n) do
			local stack = inv:get_stack(n, i)
			if sz_stuffpile and stack:get_count() > 0 then
				stack = sz_stuffpile.settle_search(pos, stack)
			end
			if stack:get_count() > 0 then
				pos:item_eject(stack)
			end
		end
		inv:set_list(n, { })
	end
end

-- Find all nearby items and try to pick them back up again.
local function pickupall(player, pos)
	if player:get_hp() < 1 then return end

	local radius = sz_pos:xyz(1.5, 1.5, 1.5)
	local minpos = pos:add(radius:neg())
	local maxpos = pos:add(radius)

	for i, v in pairs(minetest.find_nodes_in_area(
		minpos, maxpos, { "group:items_on_ground" })) do
		v = sz_pos:new(v)
		local punch = v:nodedef().on_punch
		if punch then
			punch(v, v:node_get(), player)
		else
			minetest.node_punch(v, v:node_get(), player)
		end
	end

	for i, v in pairs(minetest.get_objects_inside_radius(pos, 4)) do
		if not v:is_player() then
			local rpos = sz_pos:new(v:getpos()):round()
			if rpos.x > minpos.x and rpos.x < maxpos.x
				and rpos.y > minpos.y and rpos.y < maxpos.y
				and rpos.z > minpos.z and rpos.z < maxpos.z then
				v:punch(player, 1, { })
			end
		end
	end
end

-- Pickup all nearby items, if the local area is loaded.  If not, wait for
-- it to be loaded, and then pick up items as soon as it is.
local function pickupall_loop(player, pos)
	if pos:node_get().name == "ignore" then
		minetest.after(0, function() pickupall_loop(player, pos) end)
	else
		pickupall(player, pos)
	end
end

------------------------------------------------------------------------
-- PLAYER EVENT HANDLERS

-- When a player logs out, drop all inventory items, so they're
-- preserved in the world and not locked away in a non-corporeal
-- player.
minetest.register_on_leaveplayer(function(player)
	dropall(player)
end)

-- When shutting down the server, all players are disconnected, so
-- process drop-everything logic for all currently-connected players.
minetest.register_on_shutdown(function()
	for k, v in pairs(minetest.get_connected_players()) do
		dropall(v)
	end
end)

-- Upon logging back in, attempt to pickup all nearby items automatically,
-- so we don't end up leaving them in the wilderness and forgetting where
-- they are, after an inopportune disconnect.
minetest.register_on_joinplayer(function(player)
	local pos = sz_pos:new(player:getpos())
	minetest.after(0, function() pickupall_loop(player, pos) end)
end)
