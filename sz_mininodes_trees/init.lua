-- LUALOCALS < ---------------------------------------------------------
local ipairs, sz_mininodes
    = ipairs, sz_mininodes
-- LUALOCALS > ---------------------------------------------------------

local reg = sz_mininodes.register

sz_mininodes.reregister_defer(function()
	-- Register mininodes for the 5 different tree trunk types.
	reg("default", "tree")
	reg("default", "jungletree")
	reg("default", "acacia_tree")
	reg("default", "pine_tree")
	reg("default", "aspen_tree")

	-- Register custom texture overlays for the "cut away" sections of
	-- tree trunks, showing the rings longitudinally.
	for i, v in ipairs({
		"acacia_tree",
		"aspen_tree",
		"jungletree",
		"pine_tree",
		"tree"
		}) do
		sz_mininodes.custom_overlay_set("default_" .. v .. ".png",
			"sz_mininodes_trees_" .. v .. ".png")
	end
end)
