-- LUALOCALS < ---------------------------------------------------------
local ipairs, minetest, pairs, sz_pos, sz_table, sz_util
    = ipairs, minetest, pairs, sz_pos, sz_table, sz_util
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

------------------------------------------------------------------------
-- REGISTER SIGN GROUP

sz_util.modify_node("*", function(def, name)
	if name:startswith(":") then
		name = name:sub(1)
	end
	if name:startswith("default:sign_wall_") then
		def.groups = def.groups or { }
		def.groups.sign = 1
	end
	return def
end)

------------------------------------------------------------------------
-- PERIODICALLY SCAN NODES AND ATTACH HUDS

local function huddata(pos)
	return pos:meta():get_string("text") or ""
end

local function mkhud(player, pos)
	local cap = huddata(pos)
	local id = player:hud_add({
		hud_elem_type = "waypoint",
		world_pos = pos,
		name = cap,
		number = 0xFFFFFF
	})
	return function(keep)
		if not keep then return player:hud_remove(id) end
		local n = huddata(pos)
		if n ~= cap then player:hud_change(id, "name", n) end
		cap = n
	end
end

local seethru = {
	airlike = true,
	glasslike = true,
	mesh = true,
	nodebox = true,
	signlike = true,
}
local function lineofsight(ppos, pos)
	local vec = pos:sub(ppos)
	local dist = vec:len()
	for i = 0, 1, 0.1 / dist do
		local p = ppos:add(vec:scale(i))
		local n = p:nodedef()
		if not n or not n.drawtype or not seethru[n.drawtype] then
			return false
		end
	end
	return true
end

local allhuds = {}

local oldpos = {}

local function rescan()
	minetest.after(1, rescan)
	local keephuds = {}
	for i, player in ipairs(minetest.get_connected_players()) do
		local n = player:get_player_name()
		local ppos = sz_pos:new(player:getpos()):add(sz_pos:xyz(0, 1.5, 0))
		local opos = oldpos[n]
		oldpos[n] = ppos
		local phuds = allhuds[n] or {}
		local newhuds = {}
		if opos and opos:eq(ppos) then
			local bydist = sz_table:new()
			for i, pos in ipairs(ppos:nodes_in_area(8, "group:sign")) do
				pos = sz_pos:new(pos)
				if huddata(pos) ~= "" and lineofsight(ppos, pos) then
					local d = pos:sub(ppos)
					d = d:dot(d)
					bydist[d] = bydist[d] or { }
					bydist[d][pos:hash()] = pos
				end
			end
			local dists = bydist:keys()
			dists:sort()
			local t = 0
			for i, d in ipairs(dists) do
				for hash, pos in pairs(bydist[d]) do
					t = t + 1
					if t > 20 then break end
					local hud = phuds[hash] or mkhud(player, pos)
					newhuds[hash] = hud
					phuds[hash] = nil
					hud(true)
				end
				if t > 20 then break end
			end
		end
		for k, v in pairs(phuds) do v() end
		keephuds[n] = newhuds
	end
	allhuds = {}
	for k, v in pairs(keephuds) do
		allhuds[k] = v
	end
end
minetest.after(1, rescan)
