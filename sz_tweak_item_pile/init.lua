-- LUALOCALS < ---------------------------------------------------------
local ItemStack, minetest, sz_pos, sz_stuffpile, tonumber, tostring
    = ItemStack, minetest, sz_pos, sz_stuffpile, tonumber, tostring
-- LUALOCALS > ---------------------------------------------------------

local modname = minetest.get_current_modname()

local settletime = tonumber(minetest.setting_get(modname .. "_settletime")) or 300
local itemttl = tonumber(minetest.setting_get("item_entity_ttl"))
if itemttl and settletime > (itemttl / 2) then settletime = itemttl / 2 end

local bitemname = "__builtin:item"
local bitem = minetest.registered_entities[bitemname]

local old_step = bitem.on_step
bitem.on_step = function(self, dtime, ...)
	local pos = sz_pos:new(self.object:getpos())
	local posdef = pos:nodedef()

	-- Handle items "settling" into "pile of stuff" nodes.
	local posstr = tostring(pos)
	if self.sz_data.oldpos == posstr then
		-- If object is in the same position it was last time we checked,
		-- then add time to the "stationary" counter.
		self.sz_data.timer = (self.sz_data.timer or 0) + dtime
		if self.sz_data.timer >= settletime then
			-- If the object has been stationary for long enough, then
			-- "settle" it into nodespace.
			local left = sz_stuffpile.settle_search(pos, ItemStack(self.itemstring))
			if left:is_empty() then
				self.object:remove()
			elseif self.itemstring ~= left:to_string() then
				self.itemstring = left:to_string()
			end
			self.sz_data.timer = 0
		end
	else
		-- If the object has moved, reset the timer.
		self.sz_data.oldpos = posstr
		self.sz_data.timer = 0
	end

	return old_step(self, dtime, ...)
end

minetest.register_entity(":"..bitemname, bitem)
